/* This file is part of the QuandoScheduler Library.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

//#include "quandoschedule.h"

#include "QuandoSchedulerMain.h"
#include "QuandoSchedulerMetaData.h"

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QTextStream>
#include <QDebug>
#include <QLoggingCategory>

int main(int argc, char** argv)
{
    QLoggingCategory::setFilterRules("quandoscheduler.*=false");

    int posShortOption = 2;
    int posLongOption = 6; //includes arg
    int posDescription = 25;
    
    QTextStream out(stdout);

    QCoreApplication app(argc, argv);
    app.setApplicationName("quandoschedule");
    app.setApplicationVersion("0.0.80");

    QuandoScheduler::Main controller;

    QCommandLineParser parser;
//     parser.setOptionsAfterPositionalArgumentsMode(QCommandLineParser::ParseAsPositionalArguments);
    parser.setApplicationDescription("Quando project scheduler");
    parser.addVersionOption();
    const QCommandLineOption helpOption = parser.addHelpOption();

    QCommandLineOption progressOption(QStringList() << "p" << "progress",
                               QCoreApplication::translate("main", "Show progress during job execution."));
    parser.addOption(progressOption);

    parser.parse(app.arguments());
    const QStringList pargs = parser.positionalArguments();
    QStringList plugins = controller.availableJobs();
    if (pargs.isEmpty()) {
        // add some help
        parser.addPositionalArgument("job [options] [job ...]", QCoreApplication::translate("main", "List of jobs. The jobs will be executed in the specified sequence."));

        // If any plugins, add plugin commands as available jobs
        if (plugins.isEmpty()) {
            out << QCoreApplication::translate("main", "Sorry, no jobs available.") << endl;
            return 1;
        }
        parser.parse(app.arguments());
        QString hlp = parser.helpText();
        hlp += "\nAvailable jobs:\n";

        for (const QString &plugin : plugins) {
            QuandoScheduler::MetaData *md = controller.pluginMetaData(plugin);
            hlp = hlp.append(QString("  %1 %2\n").arg(md->command(), -15).arg(md->description()));
            delete md;
        }
        out << hlp;
        return 0;
    }
    if (parser.isSet(helpOption)) {
        if (pargs.count() > 1) {
            out << QCoreApplication::translate("main", "You can only get help for one job at a time.") << endl;
            return 0;
        }
        // check pargs agains available plugins
        parser.clearPositionalArguments();
        QuandoScheduler::MetaData *md = controller.pluginMetaData(pargs.at(0));
        if (!md) {
            out << QCoreApplication::translate("main", "Unknonw job: %1").arg(pargs.at(0)) << endl;;
            return 0;
        }
        parser.addPositionalArgument(md->command(), md->description());
        QString hlp = parser.helpText();
        hlp += '\n';
        hlp += QCoreApplication::translate("main", "Options:") + '\n';
        QMap<QString, QString> opts = md->commandOptions();
        for (QMap<QString, QString>::const_iterator it = opts.constBegin(); it != opts.constEnd(); ++it) {
            int pos = 0;
            QString line("%1");
            if (!it.value().isEmpty()) {
                line = line.arg("", posShortOption - pos);
                line += QString("-%1,").arg(it.value());
                pos = line.length();
            }
            if (pos < posLongOption) {
                line += "%1";
                line = line.arg("", posLongOption - pos);
            }
            line += QString("--%1").arg(it.key());
            pos = line.length();
            if (!md->commandOptionValueName(it.key()).isEmpty()) {
                line += QString(" <%1>").arg(md->commandOptionValueName(it.key()));
                pos = line.length();
                if (pos < posDescription) {
                    line += "%1";
                    line = line.arg("", posDescription - pos);
                }
                line += QString(" %1").arg(md->commandOptionValues(it.key()).join(','));
                if (!md->commandOptionDefaultValue(it.key()).isEmpty()) {
                    line += QString(" (Default: %1)").arg(md->commandOptionDefaultValue(it.key()));
                }
                hlp += line + '\n';
                line.clear();
                pos = 0;
            }
            if (pos < posDescription) {
                line += " %1";
                line = line.arg("", posDescription - pos);
            }
            line += QString("%1\n").arg(md->commandOptionDescription(it.key()).last());
            hlp += line;
        }
        out << hlp << endl;
        return 0;
    }
    // Get job ids/options and check if all specified jobs are available
    qDebug()<<"app args:"<<app.arguments();
    for (const QString job : pargs) {
        bool ok = false;
        int idx = app.arguments().indexOf(job);
        if (idx < 0) {
            continue;
        }
        QStringList arguments(job);
        while (++idx < app.arguments().count() && app.arguments().at(idx)[0] == '-') {
            arguments << app.arguments().at(idx);
        }
        QCommandLineParser pars;
        pars.parse(arguments);
        qDebug()<<"Job args:"<<arguments;
        for (const QString &plugin : plugins) {
            QuandoScheduler::MetaData *md = controller.pluginMetaData(plugin);
            if (job != md->command()) {
                continue;
            }
            pars.addPositionalArgument(md->command(), "");
            QString jobId = md->id();
            QVariantMap args;
            QMap<QString, QString> opts = md->commandOptions();
            for (QMap<QString, QString>::const_iterator it = opts.constBegin(); it != opts.constEnd(); ++it) {
                bool expectsValue = !md->commandOptionValueName(it.key()).isEmpty();
                QCommandLineOption opt(QStringList()<<it.key()<<it.value());
                if (expectsValue) {
                    opt.setValueName(md->commandOptionValueName(it.key()));
                    opt.setDefaultValue(md->commandOptionDefaultValue(it.key()));
                }
                pars.addOption(opt);
            }
            pars.process(arguments); //exits on error
            qDebug()<<"Pars opts:"<<pars.optionNames();
            out << pars.helpText()<<endl;
            for (const QString &o : pars.optionNames()) {
                args[o] = pars.value(o);
            }
            controller.addJob(jobId, args);

            delete md;
            ok = true;
        }
        if (!ok) {
            out << QCoreApplication::translate("main", "Unknonw job: %1").arg(job) << endl;
            return 1;
        }
    }
    // if we get here, we have some job(s) to run
    QObject::connect(&controller, &QuandoScheduler::Main::finished, &app, &QCoreApplication::quit);
    int result = 2;
    if (controller.execute()) {
        result = app.exec();
    }
    qDebug()<<"result:"<<result;
    return result;
}
