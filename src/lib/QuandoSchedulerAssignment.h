/* This file is part of the QuandoScheduler project.
   Copyright (C) 2017 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_Assignment_h
#define QuandoScheduler_Assignment_h

#include "quandoscheduler_export.h"


#include <QObject>
#include <QDateTime>
#include <QDebug>

namespace QuandoScheduler {

class Task;
class Resource;

class QUANDOSCHEDULER_EXPORT Assignment : public QObject
{
    Q_OBJECT
public:
    Assignment();
    Assignment(Task *task, Resource *resource);
    Assignment(const Assignment &other);

    bool isValid() const;    
    bool isOpen() const;
    
    void setTask(Task *task);
    Task *task() const;
    void setResource(Resource *resource);
    Resource *resource() const;
    void setAmount(double amount);
    double amount(int pos = -1) const;
    void setFrom(const QDateTime &from);
    void setUntil(const QDateTime &until);

    int count() const;
    QDateTime from(int pos) const;
    QDateTime until(int pos) const;

private:
    Task *m_task;
    Resource *m_resource;

    QList<QDateTime> m_from;
    QList<QDateTime> m_until;
    QList<double> m_amount;
};

}

QUANDOSCHEDULER_EXPORT QDebug operator<<(QDebug dbg, const QuandoScheduler::Assignment *a);

#endif // QuandoScheduler_Assignment_h
