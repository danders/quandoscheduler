/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerProject.h"

#include "QuandoSchedulerDependency.h"
#include "QuandoSchedulerTask.h"
#include "QuandoSchedulerResource.h"

#include <QVariant>

namespace QuandoScheduler {
    
Project::Project()
    : m_priority(0)
    , m_direction(Direction::Forward)
{
    m_startTask.m_project = this;
    m_startTask.setObjectName("Start");
    m_startTask.setPriority(-2);
    m_endTask.m_project = this;
    m_endTask.setObjectName("End");
    m_endTask.setPriority(-1);
}

Project::~Project()
{
}

bool Project::addTask(const QVariant &uid, Task *task)
{
    if (m_tasks.contains(uid)) {
        return false;
    }
    m_tasks[uid] = task;
    return true;
}

Task *Project::task(const QVariant &uid) const
{
    return m_tasks.value(uid);
}

bool Project::addResource(const QVariant &uid, Resource *resource)
{
    if (m_resources.contains(uid)) {
        return false;
    }
    m_resources[uid] = resource;
    return true;
}

Resource *Project::resource(const QVariant &uid) const
{
    return m_resources.value(uid);
}

Dependency *Project::addDependency(const QVariant &predeccessor, const QVariant &successor, int type, int lag)
{
    Dependency *dep = 0;
    Task *pred = m_tasks.value(predeccessor);
    Task *succ = m_tasks.value(successor);
    if (pred && succ) {
        dep = new Dependency(pred, succ, type, lag);
        pred->addSuccessor(dep);
        succ->addPredeccessor(dep);
    }
    return dep;
}

Allocation *Project::addAllocation(const QVariant &resource, const QVariant &task, double amount)
{
    Allocation *a = 0;
    return a;
}

Task *Project::findTask(const QVariant &taskId) const
{
    return m_tasks.value(taskId);
}

void Project::setSchedulingRange(const QDateTime &start, const QDateTime &end)
{
    m_start = start;
    m_end = qMax(start, end);
}

QDateTime Project::targetStart() const
{
    return m_start;
}

void Project::setTargetStart(const QDateTime &start)
{
    m_start = start;
}

QDateTime Project::targetFinish() const
{
    return m_end;
}

void Project::setTargetFinish(const QDateTime &time)
{
    m_end = time;
}

void Project::setPriority(int prio)
{
    m_priority = prio;
}

int Project::priority() const
{
    return m_priority;
}

QDateTime Project::actualStart() const
{
    return m_actualStart;
}

void Project::setActualStart(const QDateTime& dt)
{
    m_actualStart = dt;
}

QDateTime Project::actualFinish() const
{
    return m_actualFinish;
}

void Project::setActualFinish(const QDateTime& dt)
{
    m_actualFinish = dt;
}


QList<Task*> Project::tasks() const
{
    return m_tasks.values();
}

Task *Project::startTask()
{
    return &m_startTask;
}

Task *Project::endTask()
{
    return &m_endTask;
}

Direction Project::direction() const
{
    return m_direction;
}

} // namespace QuandoScheduler
