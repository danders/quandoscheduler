/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerMetaData.h"
#include "DebugLib.h"

#include <QJsonObject>
#include <QJsonArray>

namespace QuandoScheduler {

MetaData::MetaData()
{
}

MetaData::~MetaData()
{
}

void MetaData::setData(const QJsonObject &data)
{
    m_id = data.value(QLatin1String("Id")).toString();
    // TODO translated
    m_names.prepend(data.value(QLatin1String("Name")).toString());
    m_description = data.value(QLatin1String("Description")).toString();

    m_version = data.value(QLatin1String("Version")).toString();
    m_category = data.value(QLatin1String("Category")).toString();
    m_website = data.value(QLatin1String("Website")).toString();
    
    for (const QJsonValue &author : data.value(QLatin1String("Authors")).toArray()) {
        addAuthor(author.toObject());
    }
    const QJsonObject usage = data.value(QLatin1String("Usage")).toObject();
    for (const QJsonValue &option : usage.value(QLatin1String("Options")).toArray()) {
        addOption(option.toObject());
    }
}

void MetaData::addAuthor(const QJsonObject &author)
{
    const QString name = author.value(QLatin1String("Name")).toString();
    if (!name.isEmpty()) {
        const QString email = author.value(QLatin1String("Email")).toString();
        const QString info = author.value(QLatin1String("Information")).toString();
        m_authorNames.append(name);
        m_authorEmails[name] = email;
        m_authorInfo[name] = info; //TODO translated?
    }
}

void MetaData::addOption(const QJsonObject &option)
{
    debugLib<<"MetaData::addOption:"<<option;
    const QString loption = option.value(QLatin1String("Option")).toObject().value(QLatin1String("Long")).toString();
    if (!loption.isEmpty()) {
        const QString soption = option.value(QLatin1String("Option")).toObject().value(QLatin1String("Short")).toString();
        QString valueName = option.value(QLatin1String("ValueName")).toString();
        QStringList arguments;
        for (const QJsonValue &arg : option.value(QLatin1String("Values")).toArray()) {
            arguments.append(arg.toString());
        }
        const QString defaultValue = option.value(QLatin1String("Default")).toString();
        QStringList descriptions(option.value(QLatin1String("Description")).toString());
        // TODO translated
        
        m_options[loption] = soption;
        m_optionValueName[loption] = valueName;
        m_optionValues[loption] = arguments;
        m_optionDefaultValues[loption] = defaultValue;
        m_optionDesctiptions[loption] = descriptions;
    } else debugLib<<"MetaData::addOption: No Long option:"<<option;
}

QString MetaData::id() const
{
    return m_id;
}

QStringList MetaData::names() const
{
    return m_names;
}

QString MetaData::description() const
{
    return m_description;
}

QString MetaData::version() const
{
    return m_version;
}

QString MetaData::category() const
{
    return m_category;
}

QString MetaData::website() const
{
    return m_website;
}

QMap<QString, QStringList> MetaData::authors() const
{
    QMap<QString, QStringList> lst;
    for (const QString &s : m_authorNames) {
        lst[s] = QStringList() << m_authorEmails.value(s) << m_authorInfo.value(s);
    }
    return lst;
}

QString MetaData::command() const
{
    return m_id;
}

QMap<QString, QString> MetaData::commandOptions() const
{
    return m_options;
}

QString MetaData::commandOptionValueName(const QString &option) const
{
    return m_optionValueName.value(option);
}

QStringList MetaData::commandOptionValues(const QString &option) const
{
    return m_optionValues.value(option);
}

QString MetaData::commandOptionDefaultValue(const QString &option) const
{
    return m_optionDefaultValues.value(option);
}

QStringList MetaData::commandOptionDescription(const QString &option) const
{
    return m_optionDesctiptions.value(option);
}


QString MetaData::toString() const
{
    QString result;
    result += "Id: " + m_id +'\n';
    result += "Name: " + m_names.value(0);
    result += "\nVersion: " + m_version;
    result += "\nAuthors: \n";
    for (const QString &authorName : m_authorNames) {
        QString author = QString("  %1\n  %2\n  %3\n");
        result += author.arg(authorName).arg(m_authorEmails.value(authorName)).arg(m_authorInfo.value(authorName));
    }
    result += "\nUsage: \n";
    for (QMap<QString, QString>::const_iterator it = m_options.constBegin(); it !=  m_options.constEnd(); ++it) {
        QString opt = QString("\t--%1\t%2\t%3\n%4\n")
                                .arg(it.key(), -10)
                                .arg(it.value().isEmpty()?"":('-'+it.value()), -3)
                                .arg(m_optionDesctiptions.value(it.key()).last())
                                .arg("\t\tValue name: " + m_optionValueName.value(it.key()))
                                .arg("\t\tValues: " + m_optionValues.value(it.key()).join(", "));

        result += opt;
    }
//         QMap<QString, QString> m_optionDefaultValues;

    if (!result.endsWith('\n')) {
        result += '\n';
    }
    return result;
}

} // namespace QuandoScheduler
