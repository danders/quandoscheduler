/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_Dependency_h
#define QuandoScheduler_Dependency_h

#include "quandoscheduler_export.h"

#include "QuandoScheduler.h"

#include <QObject>
#include <QVariant>
#include <QDebug>

namespace QuandoScheduler {

class Task;
class Available;

class QUANDOSCHEDULER_EXPORT Dependency : public QObject
{
    Q_OBJECT
public:
    Dependency();
    /// Creates a Dependency between @p predeccessor task and @p successor task
    /// of type @p type with a @p lag in seconds.
    Dependency(Task *predeccessor, Task *successor, int type = DT_FinishStart, int lag = 0);

    ~Dependency();

    /// Predeccessor task
    Task *predeccessor() const;
    /// Successor task
    Task *successor() const;
    /// Type of dependency
    int type() const;
    /// Returns lag in hours
    int lag() const;
    /// Set lag to @p duration in hours
    void setLag(double duration);

    void addAvailability(int type, const QDateTime& from, const QDateTime& until);
    QList<Available*> availabilities() const;
    
    void detach();
    void atach();

private:
    Task *m_predeccessor;
    Task *m_successor;
    int m_type;
    int m_lag;
    QList<Available*> m_availability; // defines actual length of lag
};

} // namespace QuandoScheduler

QUANDOSCHEDULER_EXPORT QDebug operator<<(QDebug dbg, QuandoScheduler::Dependency *dep);

#endif // QuandoScheduler_Dependency_h
