/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_Resource_h
#define QuandoScheduler_Resource_h

#include "quandoscheduler_export.h"

#include "QuandoScheduler.h"

#include <QObject>
#include <QDateTime>
#include <QList>
#include <QMap>

class QVariant;

namespace QuandoScheduler {

class Available;
class Allocation;
class Task;
class Data;

class QUANDOSCHEDULER_EXPORT Resource : public QObject
{
    Q_OBJECT
public:
    Resource(Data &data);
    ~Resource();

    int type() const;
    void setType(int type);

    /// Returns the resources basic available amount
    double amount() const;
    /// The amount this resource is available, normally 1.0
    /// If available 50%, amount = 0.5, if available 300% amount = 3.0
    void setAmount(double amount);

    /// Set resource availability to @p type for the interval @p from - @p until.
    /// Set @p amount to the amount the resource is available for this interval.
    /// Normally amount is 1.0 meaning resource is fully available.
    /// Also @see setAmount()
    void addAvailability(int type, const QDateTime &from, const QDateTime &until, double amount = 1.0);

    /// Return list of availability intervals
    QList<Available*> availabilities() const;

    QDateTime zeroTime() const;
    QDateTime maxTime() const;

private:
    QString m_id;
    Data &m_data;
    int m_type;
    double m_amount;
    QList<Available*> m_availability;
};

}

QUANDOSCHEDULER_EXPORT QDebug operator<<(QDebug dbg, const QuandoScheduler::Resource *r);

#endif // QuandoScheduler_Resource_h
