/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerMain.h"

#include "QuandoSchedulerProject.h"
#include "QuandoSchedulerPluginInterface.h"
#include "DebugLib.h"

#include <QThread>
#include <QVariantMap>
#include <QTimer>

namespace QuandoScheduler {

class Thread : public QThread
{
    Q_OBJECT
public:
    Thread(QObject *parent = 0)
    : QThread(parent)
    , job(0)
    {}

    ~Thread()
    {
        if (job && !isFinished()) {
            // TODO terminate job
        }
    }

    PluginInterface *job;

protected:
    void run() Q_DECL_OVERRIDE {
        job->execute();
    }
};

Main::Main()
    : m_currentJob(-1)
    , m_thread(new Thread(this))
{
    connect(m_thread, &QThread::started, this, &Main::threadStarted);
    connect(m_thread, &QThread::finished, this, &Main::slotJobFinished);
    m_timer.setSingleShot(true);
    connect(&m_timer, &QTimer::timeout, this, &Main::threadTimeout);
}

Main::~Main()
{
    m_thread->quit();
    m_thread->wait();
}

QStringList Main::availableJobs() const
{
    return m_manager.plugins();
}

MetaData *Main::pluginMetaData(const QString &id) const
{
    return m_manager.metaData(id);
}

QStringList Main::jobs() const
{
    return m_jobs;
}

void Main::addJob(const QString &job, const QVariantMap &args)
{
    debugLib<<job<<args;
    m_jobs.append(job);
    m_args.append(args);
}

Data &Main::data()
{
    return m_data;
}

const Data &Main::data() const
{
    return m_data;
}

bool Main::execute()
{
    bool result = false;
    if (m_currentJob == -1) {
        result = m_data.prepare();
        if (result) {
            result = executeNext();
        }
    }
    return result;
}

bool Main::executeNext()
{
    debugLib;
    ++m_currentJob;
    if (m_jobs.count() > m_currentJob) {
        PluginInterface *pl = m_manager.plugin(m_jobs.at(m_currentJob));
        if (!pl) {
            return false;
        }
        debugLib<<m_currentJob;
        if (!pl->initiate(this, m_data, m_args.value(m_currentJob))) {
            return false;
        }
        m_thread->job = pl;
        m_timer.start(4000); // 4 seconds timeout
        m_thread->start();
        return true;
    }
    return false;
}

void Main::slotJobProgress(int progress)
{
    debugLib<<progress;
    if (progress > 70) {
        m_thread->requestInterruption();
    }
}

void Main::slotJobMessage(const QString &msg)
{
    debugLib<<msg;
}

void Main::slotJobFinished()
{
    debugLib<<m_currentJob<<m_jobs.value(m_currentJob);
    // check for errors
    if (m_thread->job->state() != 0) {
        debugLib<<"Error:"<<m_currentJob<<m_jobs.value(m_currentJob)<<m_thread->job->state();
        m_currentJob = -1;
        m_thread->job = 0;
        emit finished();
        return;
    }
    // execute next job (if any)
    if (!executeNext()) {
        // no morejobs
        m_currentJob = -1;
        m_thread->job = 0;
        emit finished();
    }
}

void Main::threadTimeout()
{
    debugLib;
    m_thread->quit();
    m_thread->wait();
    emit finished();
}

void Main::threadStarted()
{
    m_timer.stop();
}


} // namespace QuandoScheduler

#include "QuandoSchedulerMain.moc"
