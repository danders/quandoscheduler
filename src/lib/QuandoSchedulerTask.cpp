/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerTask.h"
#include "QuandoScheduler.h"
#include "QuandoSchedulerProject.h"
#include "QuandoSchedulerAllocation.h"
#include "QuandoSchedulerAvailable.h"
#include "QuandoSchedulerDependency.h"

#include <QVariant>

namespace QuandoScheduler {

Task::Task()
    : QObject()
    , m_project(0)
    , m_type(TT_Task)
    , m_priority(0)
    , m_schedulingType(ST_ASAP)
    , m_targetType(TT_None)
    , m_estimateType(ET_Effort)
{
}

Task::Task(Project *project, Task *p)
: QObject(p)
, m_project(project)
, m_type(TT_Task)
, m_priority(0)
, m_schedulingType(ST_ASAP)
, m_targetType(TT_None)
, m_estimateType(ET_Effort)
{
}

Task::~Task()
{
    while (!m_predeccessors.isEmpty()) {
        delete m_predeccessors.takeFirst(); // remove first, then delete or else ~Dependency() fails
    }
    while (!m_successors.isEmpty()) {
        delete m_successors.takeFirst(); // remove first, then delete or else ~Dependency() fails
    }
    qDeleteAll(m_availability);
}

Project *Task::project() const
{
    return m_project;
}

Task *Task::parentTask() const
{
    return qobject_cast<Task*>(parent());
}

void Task::addAllocation(Allocation *allocation)
{
    m_allocations.insert(allocation->resource(), allocation);
}

QMap<Resource*, Allocation*> Task::allocations() const
{
    return m_allocations;
}

void Task::setType(int type)
{
    m_type = type;
}

int Task::type() const
{
    return m_type;
}

void Task::setPriority(int prio)
{
    m_priority = prio;
}

int Task::priority() const
{
    return m_priority;
}

void Task::setSchedulingType(int type)
{
    m_schedulingType = type;
}

int Task::schedulingType() const
{
    return m_schedulingType;
}

int Task::targetType() const
{
    return m_targetType;
}

void Task::setTargetType(int type)
{
    m_targetType = type;
}

void Task::setTargetStart(const QDateTime& dt)
{
    m_targetStart = dt;
}

void Task::setTargetFinish(const QDateTime& dt)
{
    m_targetFinish = dt;
}

QDateTime Task::targetStart() const
{
    return m_targetStart;
}

QDateTime Task::targetFinish() const
{
    return m_targetFinish;
}

void Task::setEstimateType(int type)
{
    m_estimateType = type;
}

int Task::estimateType() const
{
    return m_estimateType;
}

void Task::setEstimate(double estimate)
{
    m_estimate = estimate;
}

double Task::estimate() const
{
    return m_estimate;
}

void Task::addPredeccessor(Dependency* dep)
{
    m_predeccessors.append(dep);
}

void Task::removePredeccessor(Dependency *dep)
{
    int idx = m_predeccessors.indexOf(dep);
    if (idx >= 0) {
        m_predeccessors.removeAt(idx);
    }
}

QList<Dependency*> Task::predeccessors() const
{
    return m_predeccessors;
}

void Task::addSuccessor(Dependency* dep)
{
    m_successors.append(dep);
}

void Task::removeSuccessor(Dependency *dep)
{
    int idx = m_successors.indexOf(dep);
    if (idx >= 0) {
        m_successors.removeAt(idx);
    }
}

void Task::setAvailabilities(const QList<Available*> &lst)
{
    for (const Available *a : lst) {
        m_availability.append(new Available(*a));
    }
}

void Task::addAvailability(int type, const QDateTime& from, const QDateTime& until)
{
    Available *a = new Available();
    a->type = type;
    a->from = from;
    a->until = until;
    m_availability.append(a);
}

QList<Available*> Task::availabilities() const
{
    return m_availability;
}

QList<Dependency*> Task::successors() const
{
    return m_successors;
}

QDateTime Task::actualStart() const
{
    return m_actualStart;
}

void Task::setActualStart(const QDateTime& dt)
{
    m_actualStart = dt;
}

QDateTime Task::actualFinish() const
{
    return m_actualFinish;
}

void Task::setActualFinish(const QDateTime& dt)
{
    m_actualFinish = dt;
}

void Task::addAssignment(Resource *resource, const Assignment &assignment)
{
    m_assignments.insert(resource, assignment);
}

const QMultiMap<Resource*, Assignment> Task::assignments() const
{
    return m_assignments;
}

} // namespace QuandoScheduler
