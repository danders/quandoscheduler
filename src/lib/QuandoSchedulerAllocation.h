/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_Allocation_h
#define QuandoScheduler_Allocation_h

#include "quandoscheduler_export.h"

#include <QObject>
#include <QDebug>

namespace QuandoScheduler {

class Resource;
class Task;

class QUANDOSCHEDULER_EXPORT Allocation : public QObject
{
    Q_OBJECT
public:
    Allocation();
    Allocation(Task *task, QList<Resource*> resources, double amount);

    Resource *resource() const;
    Task *task() const;
    /// The amount is how much of the resource that will be assigned to the task
    double amount() const;

    void setMandatory(bool state);
    bool isMandatory() const;

private:
    Task *m_task;
    double m_amount;
    bool m_mandatory;

    QList<Resource*> m_alternatives;
    
};

} // namespace Quando

QUANDOSCHEDULER_EXPORT QDebug operator<<(QDebug dbg, const QuandoScheduler::Allocation *a);

#endif // QuandoScheduler_Allocation_h
