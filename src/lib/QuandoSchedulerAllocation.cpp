/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerAllocation.h"


namespace QuandoScheduler {

Allocation::Allocation()
    : m_task(0)
    , m_amount(0)
    , m_mandatory(false)
{
}

Allocation::Allocation(Task *t, QList<Resource*> resources, double a)
    : m_task(t)
    , m_amount(a)
    , m_alternatives(resources)
{
}

Resource *Allocation::resource() const
{
    return m_alternatives.value(0);
}

Task *Allocation::task() const
{
    return m_task;
}

double Allocation::amount() const
{
    return m_amount;
}

void Allocation::setMandatory(bool state)
{
    m_mandatory = state;
}

bool Allocation::isMandatory() const
{
    return m_mandatory;
}

} // namespace QuandoScheduler

QDebug operator<<(QDebug dbg, const QuandoScheduler::Allocation *a)
{
    dbg.nospace() << "Allocation[";
    if (a) {
        dbg << a->resource() << a->task() << ',' << a->amount();
    } else {
        dbg << (void*)a;
    }
    dbg << ']';
    return dbg.space();
}
