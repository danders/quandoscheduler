/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_PluginManager_h
#define QuandoScheduler_PluginManager_h

#include <QMap>

class QString;
class QStringList;
class QPluginLoader;
class QJsonObject;

namespace QuandoScheduler {

class PluginInterface;
class MetaData;

class PluginManager
{
public:
    PluginManager();
    ~PluginManager();
    
    /// return a list of available plugins
    QStringList plugins() const;

    /// Get the plugin with identity @p id.
    /// The plugin is loaded if necessary.
    /// Returns 0 if unknown @p id
    PluginInterface *plugin(const QString &id);

    /// Returns all metadata for the plugin with id @pluginId, or
    /// if @p scriptId is set, returns the scripts metadata
    MetaData *metaData(const QString &pluginId, const QString &scriptId = QString()) const;

    /// Returns all metadata for the plugin with id @pluginId, or
    /// if @p scriptId is set returns the scripts metadata
    QJsonObject jsonData(const QString &pluginId, const QString &scriptId = QString()) const;

protected:
    /// return a list of available plugins
    QStringList plugins(const QString &path) const;

private:
    /// List of open plugin loaders, sorted by Category
    mutable QMap<QString, QMap<QString, QPluginLoader*> > m_loaders;
};

} // namespace QuandoScheduler

#endif // QuandoScheduler_PluginManager_h
