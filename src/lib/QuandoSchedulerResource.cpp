/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerResource.h"
#include "QuandoSchedulerData.h"
#include "QuandoSchedulerAllocation.h"
#include "QuandoSchedulerAvailable.h"

#include <QVariant>

namespace QuandoScheduler {
    
Resource::Resource(Data &data)
    : m_data(data)
    , m_type(RT_Work)
    , m_amount(1.0)
{
}

Resource::~Resource()
{
    qDeleteAll(m_availability);
}

void Resource::setType(int type)
{
    m_type = type;
}

int Resource::type() const
{
    return m_type;
}

void Resource::setAmount(double amount)
{
    m_amount = amount;
}

double Resource::amount() const
{
    return m_amount;
}

void Resource::addAvailability(int type, const QDateTime& from, const QDateTime& until, double amount)
{
    Available *a = new Available();
    a->type = type;
    a->from = from;
    a->until = until;
    a->amount = amount;
    m_availability.append(a);
}

QList<Available*> Resource::availabilities() const
{
    return m_availability;
}

QDateTime Resource::zeroTime() const
{
    m_data.zeroTime();
}

QDateTime Resource::maxTime() const
{
    m_data.maxTime();
}

} // namespace QuandoScheduler

QDebug operator<<(QDebug dbg, const QuandoScheduler::Resource *r)
{
    dbg.nospace() << "Resource[";;
    if (r) {
        if (r->objectName().isEmpty()) {
            dbg << (void*)r;
        } else {
            dbg << r->objectName();
        }
    } else {
        dbg.nospace() << "0x0";
    }
    dbg << ']';
    return dbg.space();
}
