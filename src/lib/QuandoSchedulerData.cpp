/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerData.h"
#include "QuandoScheduler.h"
#include "QuandoSchedulerProject.h"
#include "QuandoSchedulerTask.h"
#include "QuandoSchedulerResource.h"
#include "QuandoSchedulerAllocation.h"
#include "QuandoSchedulerDependency.h"
#include "DebugLib.h"

#include <QVariantList>

namespace QuandoScheduler {

Data::Data()
    : m_granularity(5*60)
{
}

Data::~Data()
{
    qDeleteAll(m_projects);
    qDeleteAll(m_tasks);
    qDeleteAll(m_resources);
}

bool Data::prepare()
{
    if (m_projects.isEmpty()) {
        // No data at all is valid
        return true;
    }
    bool result = true;
    if (result) {
        for (Project *p : m_projects) {
            // All projects must be valid
            if (p->m_tasks.isEmpty() || !p->m_start.isValid() || !p->m_end.isValid()) {
                result = false;
                break;
            }
            if (!m_start.isValid() || m_start > p->m_start) {
                m_start = p->m_start;
            }
            if (!m_end.isValid() || m_end < p->m_end) {
                m_end = p->m_end;
            }
            p->m_startTask.setTargetStart(p->targetStart());
            p->m_startTask.setTargetType(TT_StartNotEarlier);
            
            p->m_endTask.setTargetFinish(p->targetFinish());
            p->m_endTask.setTargetType(TT_FinishNotLater);
        }
    }
    debugLib<<Q_FUNC_INFO<<zeroTime()<<maxTime();
    result = m_start.isValid() && m_end.isValid() && m_end > m_start;
    if (result) {
        for (Task *t : m_tasks) {
            if (t->predeccessors().isEmpty()) {
                Dependency *dep = new Dependency(&t->project()->m_startTask, t, DT_FinishStart, 0);
                dep->atach();
            }
            if (t->successors().isEmpty()) {
                Dependency *dep = new Dependency(t, &t->project()->m_endTask, DT_FinishStart, 0);
                dep->atach();
            }
            QList<Dependency*> erase;
            for (Dependency *dep : t->successors()) {
                if (dep->lag() > 0.0) {
                    Task *dt = new Task(t->project());
                    dt->project()->m_lagTasks.append(dt);
                    dt->setEstimateType(ET_Duration);
                    dt->setEstimate(dep->lag());
                    if (!dep->availabilities().isEmpty()) {
                        dt->setEstimateType(ET_Length);
                        dt->setAvailabilities(dep->availabilities());
                    }
                    Dependency *dependency = new Dependency(t, dt, DT_FinishStart);
                    dependency->atach();
                    dependency = new Dependency(dt, dep->successor(), DT_FinishStart);
                    dependency->atach();
                    erase << dep; // Not needed anymore
                }
            }
            for (Dependency *dep : erase) {
                dep->detach();
                delete dep;
            }
        }
    }
    return result;
}

void Data::setGranularity(int secs)
{
    m_granularity = qMax(60, secs);
}

int Data::granularity() const
{
    return m_granularity;
}

Project *Data::createProject(const QVariant &uid)
{
    Project *p = 0;
    if (uid.isValid() && !m_projects.contains(uid)) {
        p = new Project();
        p->setObjectName(uid.toString().toLocal8Bit());
        m_projects[uid] = p;
        p->setObjectName(uid.toString());
    }
    return p;
}

Task *Data::createTask(const QVariant &uid, const QVariant &project, const QVariant &parent)
{
    Task *t = 0;
    Project *proj = m_projects.value(project);
    if (uid.isValid() && proj && !taskExists(uid, proj)) {
        Task *p = findTask(parent);
        t = new Task(proj, p);
        t->setObjectName(uid.toString().toLocal8Bit());
        if (!proj->addTask(uid, t)) {
            Q_ASSERT(false);
        }
        t->setObjectName(uid.toString());
    }
    return t;
}

Resource *Data::createResource(const QVariant &uid, const QVariant &project)
{
    Resource *r = 0;
    if (uid.isValid() && !resourceExists(uid) && (!project.isValid() || m_projects.contains(project))) {
        r = new Resource(*this);
        r->setObjectName(uid.toString().toLocal8Bit());
        if (project.isValid()) {
            m_projects[project]->addResource(uid, r);
        } else {
            // global resource
            m_resources[uid] = r;
        }
        r->setObjectName(uid.toString());
    }
    return r;
}

Dependency *Data::createDependency(const QVariant &project, const QVariant &predeccessor, const QVariant &successor, int type, int lag)
{
    Dependency *dep = 0;
    Project *p = m_projects.value(project);
    if (p) {
        dep = p->addDependency(predeccessor, successor, type, lag);
    }
    return dep;
}

Allocation *Data::createAllocation(const QVariant &project, const QVariant &task, const QVariant &resource, double amount, bool mandatory)
{
    return createAllocation(project, task, QVariantList()<<resource, amount, mandatory);
}

Allocation *Data::createAllocation(const QVariant &project, const QVariant &task, const QVariantList &alternatives, double amount, bool mandatory)
{
    Allocation *a = 0;
    Project *p = m_projects.value(project);
    if (p) {
        Task *t = p->findTask(task);
        if (t) {
            QList<Resource*> resources;
            for (const QVariant &id : alternatives) {
                Resource *r = findResource(id);
                if (!r) {
                    qWarning()<<"Could not find resource:"<<id;
                    break;
                }
                resources << r;
            }
            if (!resources.isEmpty()) {
                a = new Allocation(t, resources, amount);
                a->setMandatory(mandatory);
                t->addAllocation(a);
            }
        }
    } else qWarning()<<"Could not find task:"<<task;
    debugLib<<a;
    return a;
}

void Data::setZeroTime(const QDateTime &dt)
{
    m_zeroTime = dt;
}

QDateTime Data::zeroTime() const
{
    return m_zeroTime;
}

void Data::setMaxTime(const QDateTime &dt)
{
    m_maxTime = dt;
}

QDateTime Data::maxTime() const
{
    return m_maxTime;
}

Resource *Data::findResource(const QVariant &uid) const
{
    Resource *r = m_resources.value(uid);
    if (!r) {
        for (const Project *p : m_projects) {
            r = p->resource(uid);
            if (r) {
                break;
            }
        }
    }
    return r;
}

bool Data::resourceExists(const QVariant &uid) const
{
    return findResource(uid);
}

Task *Data::findTask(const QVariant &uid, const Project *project) const
{
    Task *t = 0;
    if (project) {
        t = project->task(uid);
    } else {
        for (const Project *p : m_projects) {
            t = p->task(uid);
            if (t) {
                break;
            }
        }
    }
    return t;
}

bool Data::taskExists(const QVariant &uid, const Project *project) const
{
    return findTask(uid, project);
}

QList<Project*> Data::projects() const
{
    return m_projects.values();
}

void Data::addAssignments(const QList<Assignment*> &assignments)
{
    m_assignments += assignments;
}

QList<Assignment*> Data::assignments() const
{
    return m_assignments;
}

Assignment *Data::assignment(const Task *task, const Resource *resource) const
{
    for (Assignment *a : m_assignments) {
        if (a->task() == task && a->resource() == resource) {
            return a;
        }
    }
    return 0;
}

} // namespace QuandoScheduler
