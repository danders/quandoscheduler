/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_Main_h
#define QuandoScheduler_Main_h

#include "quandoscheduler_export.h"

#include "QuandoSchedulerData.h"
#include "QuandoSchedulerPluginManager.h"

#include <QObject>
#include <QMap>
#include <QVariant>
#include <QTimer>


namespace QuandoScheduler {

class Project;
class Task;
class Resource;
class Thread;

class QUANDOSCHEDULER_EXPORT Main : public QObject
{
    Q_OBJECT
public:
    /// Create a new scheduler
    Main();
    /// Deletes the scheduler
    ~Main();

public Q_SCRIPTABLE:
    /// returns the scheduler data
    Data &data();
    /// returns the scheduler data
    const Data &data() const;

    QStringList availableJobs() const;
    MetaData *pluginMetaData(const QString &id) const;

    QStringList jobs() const;
    void addJob(const QString &job, const QVariantMap &args);

    /// Execute the jobs
    /// Returns false if no jobs to execute
    bool execute();

public Q_SLOTS:
    /// The jobs progress is reported here
    void slotJobProgress(int);
    /// Any messages during job execution is reported here
    void slotJobMessage(const QString &msg);

Q_SIGNALS:
    void progress(int);
    void finished();

protected Q_SLOTS:
    void slotJobFinished();

private Q_SLOTS:
    void threadTimeout();
    void threadStarted();

protected:
    /// Execute next job
    bool executeNext();

private:
    PluginManager m_manager;
    Data m_data;
    QStringList m_jobs;
    QList<QVariantMap> m_args;
    int m_currentJob;

    Thread *m_thread;
    QTimer m_timer;
};

} // namespace QuandoScheduler

#endif // QuandoScheduler_Main_h
