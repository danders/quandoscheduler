/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_Task_h
#define QuandoScheduler_Task_h

#include "quandoscheduler_export.h"

#include "QuandoSchedulerAssignment.h"

#include <QObject>
#include <QList>
#include <QMap>
#include <QDateTime>

class QVariant;

namespace QuandoScheduler {

class Available;
class Allocation;
class Dependency;
class Resource;
class Project;

class QUANDOSCHEDULER_EXPORT Task : public QObject
{
    Q_OBJECT
public:
    Task();
    Task(Project *project, Task *parent = 0);
    ~Task();

    Project *project() const;
    Task *parentTask() const;

    void addAllocation(Allocation *allocation);
    QMap<Resource*, Allocation*> allocations() const;

    int type() const;
    void setType(int type);

    int priority() const;
    void setPriority(int prio);

    int schedulingType() const;
    void setSchedulingType(int type);

    int targetType() const;
    void setTargetType(int type);
    
    QDateTime targetStart() const;
    void setTargetStart(const QDateTime &dt);
    QDateTime targetFinish() const;
    void setTargetFinish(const QDateTime &dt);
    
    int estimateType() const;
    void setEstimateType(int type);
    double estimate() const; // estimate in hours
    void setEstimate(double estimate);
    void addPredeccessor(Dependency *dep);
    void removePredeccessor(Dependency *dep);
    QList<Dependency*> predeccessors() const;
    void addSuccessor(Dependency *dep);
    void removeSuccessor(Dependency *dep);
    QList<Dependency*> successors() const;

    void setAvailabilities(const QList<Available*> &lst);
    void addAvailability(int type, const QDateTime& from, const QDateTime& until);
    QList<Available*> availabilities() const;

    QDateTime actualStart() const;
    void setActualStart(const QDateTime &dt);
    QDateTime actualFinish() const;
    void setActualFinish(const QDateTime &dt);

    void addAssignment(Resource *resource, const Assignment &assignment);
    const QMultiMap<Resource*, Assignment> assignments() const;

private:
    QString m_id;
    friend class Project;
    Project *m_project;
    int m_type;
    int m_priority;
    int m_schedulingType;
    int m_targetType;
    QDateTime m_targetStart;
    QDateTime m_targetFinish;
    int m_estimateType;
    double m_estimate;
    QList<Dependency*> m_predeccessors;
    QList<Dependency*> m_successors;
    /// Mandatory allocations
    QMap<Resource*, Allocation*> m_mandatories;
    /// Normal allocations
    QMap<Resource*, Allocation*> m_allocations;
    /// Number of alternative allocations
    int m_alternativeAllocs;
    /// List of possible alternative allocations
    QList<Allocation*> m_alternatives;
    QList<Available*> m_availability; // used when estimate type is Length
    
    QDateTime m_actualStart;
    QDateTime m_actualFinish;
    QMultiMap<Resource*, Assignment> m_assignments;
};

} // namespace QuandoScheduler

#endif // QuandoScheduler_Task_h
