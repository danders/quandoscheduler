/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_Project_h
#define QuandoScheduler_Project_h

#include "quandoscheduler_export.h"

#include "QuandoScheduler.h"
#include "QuandoSchedulerTask.h"

#include <QObject>
#include <QDateTime>
#include <QMap>
#include <QList>

class QVariant;

namespace QuandoScheduler {

class Resource;
class Task;
class Dependency;
class Allocation;

class QUANDOSCHEDULER_EXPORT Project : public QObject
{
    Q_OBJECT
public:
    Project();
    ~Project();

    bool addTask(const QVariant &uid, Task *task);
    Task *task(const QVariant &uid) const;
    bool addResource(const QVariant &uid, Resource *resource);
    Resource *resource(const QVariant &uid) const;
    
    Dependency *addDependency(const QVariant &predeccessor, const QVariant &successor, int type, int lag);

    Allocation *addAllocation(const QVariant &resource, const QVariant &task, double amount = 1.0);

    Task *findTask(const QVariant &taskId) const;

    void setSchedulingRange(const QDateTime &start, const QDateTime &end);
    QDateTime targetStart() const;
    void setTargetStart(const QDateTime &start);
    QDateTime targetFinish() const;
    void setTargetFinish(const QDateTime &end);
    void setPriority(int prio);
    int priority() const;

    QDateTime actualStart() const;
    void setActualStart(const QDateTime &dt);
    QDateTime actualFinish() const;
    void setActualFinish(const QDateTime &dt);

    QList<Task*> tasks() const;
    Task *startTask();
    Task *endTask();

    Direction direction() const;

private:
    QString m_id;
    friend class Data;
    QDateTime m_start;
    QDateTime m_end;
    int m_priority;
    Direction m_direction;
    
    QDateTime m_actualStart;
    QDateTime m_actualFinish;

    Task m_startTask;
    Task m_endTask;
    QList<Task*> m_lagTasks;
    QMap<QVariant, Task*> m_tasks;
    QMap<QVariant, Resource*> m_resources;
    QList<Dependency*> m_dependencies;
};

} // namespace QuandoScheduler

#endif // QuandoScheduler_Project_h
