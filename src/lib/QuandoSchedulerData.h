/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_Data_h
#define QuandoScheduler_Data_h

#include "quandoscheduler_export.h"

#include "QuandoScheduler.h"

#include <QObject>
#include <QMap>
#include <QDateTime>
#include <QVariant>

namespace QuandoScheduler {

class Project;
class Task;
class Resource;
class Dependency;
class Allocation;
class Assignment;

class QUANDOSCHEDULER_EXPORT Data : public QObject
{
    Q_OBJECT
public:
    /// Create a new scheduler
    Data();
    /// Deletes the scheduler
    ~Data();

    bool prepare();
    
public Q_SCRIPTABLE:
    /// Set scheduling granularity. Minimum is 1 minutes. Default is 5 minutes.
    void setGranularity(int secs);
    /// Return granularity in seconds
    int granularity() const;
    
    /// Creates and adds a project with identity @p uid
    /// returns the project, or 0 if a project with identity @p uid already exists
    Project *createProject(const QVariant &uid);

    /// Creates and adds a task with identity @p uid, in @p project, with @p parent
    /// returns the task, or 0 if a task with identity @p uid already exists
    Task *createTask(const QVariant &uid, const QVariant &project, const QVariant &parent = QVariant());
    /// Creates and adds a resource with identity @p uid, in @p project

    /// Creates and adds a resource with identity @p uid
    /// If @p project is invalid, a global resource is created
    /// returns the resource, or 0 if a resource with identity @p uid already exists
    Resource *createResource(const QVariant &uid, const QVariant &project = QVariant());

    /// Creates a dependency between the tasks @p predeccessor and @p successor of @p type
    /// @p type can be:
    /// 0: Finish-Start (Default)
    /// 1: Start-Start
    /// 2: Finish-Finish
    /// 3: Start-Finish is not supported
    Dependency *createDependency(const QVariant &project, const QVariant &predeccessor, const QVariant &successor, int type = DT_FinishStart, int lag = 0);
    /// Creates and allocates the @p amount of the @p resource to the @p task
    Allocation *createAllocation(const QVariant &project, const QVariant &task, const QVariant &resource, double amount = 1.0, bool mandatory = false);
    /// Creates and allocates the @p amount of the @p resource to the @p task
    Allocation *createAllocation(const QVariant &project, const QVariant &task, const QVariantList &alternatives, double amount = 1.0, bool mandatory = false);
    
    /// Returns the list of all projects
    QList<Project*> projects() const;

    void setZeroTime(const QDateTime &dt);
    QDateTime zeroTime() const;
    void setMaxTime(const QDateTime &dt);
    QDateTime maxTime() const;

    Resource *findResource(const QVariant &uid) const;
    bool resourceExists(const QVariant &uid) const;
    Task *findTask(const QVariant &uid, const Project *project = 0) const;
    bool taskExists(const QVariant &uid, const Project *project = 0) const;

    void addAssignments(const QList<Assignment*> &assignments);
    QList<Assignment*> assignments() const;
    Assignment *assignment(const Task *task, const Resource *resource) const;

private:
    /// Scheduling granularity in seconds
    int m_granularity;
    QDateTime m_zeroTime;
    QDateTime m_maxTime;
    /// Calculated scheduling range
    QDateTime m_start;
    QDateTime m_end;
    /// The collection of projects
    QMap<QVariant, Project*> m_projects;
    /// The collection of tasks
    QMap<QVariant, Task*> m_tasks;
    /// Global resources
    QMap<QVariant, Resource*> m_resources;

    QList<Assignment*> m_assignments;
};

}

#endif /*QuandoScheduler_Data_h*/
