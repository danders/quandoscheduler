/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_h
#define QuandoScheduler_h



namespace QuandoScheduler {

    enum DependencyType {
        DT_FinishStart = 0,
        DT_FinishFinish,
        DT_StartStart,
        DT_StartFinish // not supported
    };

    enum Direction {
        Forward,
        Backward
    };

    enum AvailabilityType {
        AT_Unavailable,
        AT_Vacation,
        AT_TimeOff,
        AT_Available,
        AT_Booking
    };

    enum TaskType {
        TT_Task,
        TT_Milestone,
        TT_SummaryTask,
        TT_Repetitive
    };

    enum SchedulingType {
        ST_ASAP,
        ST_ALAP
    };

    enum TargetType {
        TT_None,
        TT_MustStartOn,
        TT_MustFinishOn,
        TT_StartNotEarlier,
        TT_FinishNotLater,
        TT_Fixed
    };

    enum EstimateType {
        ET_Effort,
        ET_Duration,
        ET_Length
    };

    enum ResourceType {
        RT_Work,        /// Effort producing, renewable resource. Availability is defined by a resource calendar.
        RT_Team,        /// TODO: Team consists of other working resources.
        RT_NonWork,     /// Renewable resource (e.g: Tool). Availability can be restricted by a resource calendar.
        RT_Consumable   /// Non-renewable resource (e.g: paint), can be replenished.
    };

} // namespace QuandoScheduler

#endif // QuandoScheduler_h
