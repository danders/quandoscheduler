/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_Available_h
#define QuandoScheduler_Available_h

#include "quandoscheduler_export.h"


#include <QObject>
#include <QDateTime>

class QVariant;

namespace QuandoScheduler {

class QUANDOSCHEDULER_EXPORT Available : public QObject
{
    Q_OBJECT
public:
    Available();
    Available(const Available &other);

    QDateTime from;
    QDateTime until;
    double amount;
    int type; // @see AvailabilityTypes
};

}

#endif // QuandoScheduler_Available_h
