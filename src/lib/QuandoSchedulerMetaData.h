/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_MetaData_h
#define QuandoScheduler_MetaData_h

#include "quandoscheduler_export.h"

#include <QObject>
#include <QMap>

class QJsonObject;

namespace QuandoScheduler {

class QUANDOSCHEDULER_EXPORT MetaData : public QObject
{
    Q_OBJECT
public:
    MetaData();
    ~MetaData();

    void setData(const QJsonObject &data);
    
    void addAuthor(const QJsonObject &author);
    void addOption(const QJsonObject &option);

    QString id() const;
    QStringList names() const;
    QString description() const;
    QString version() const;
    QString category() const;
    QString website() const;
    QMap<QString, QStringList> authors() const;

    QString command() const;
    QMap<QString, QString> commandOptions() const;
    QString commandOptionValueName(const QString &option) const;
    QStringList commandOptionValues(const QString &option) const;
    QString commandOptionDefaultValue(const QString &option) const;
    QStringList commandOptionDescription(const QString &option) const;

    QString toString() const;
private:
    QString m_id;
    QStringList m_names;  // first(): untranslated, last(): best translation
    QString m_description;  // translated description
    QString m_version;
    QString m_category;
    QString m_website;
    QStringList m_authorNames;
    QMap<QString, QString> m_authorEmails;
    QMap<QString, QString> m_authorInfo; // Like role or any other interesting info

    QMap<QString, QString> m_options;
    QMap<QString, QString> m_optionValueName;
    QMap<QString, QStringList> m_optionValues;
    QMap<QString, QString> m_optionDefaultValues;
    QMap<QString, QStringList> m_optionDesctiptions;  // first(): untranslated, last(): best translation
};

} // namespace QuandoScheduler

#endif // QuandoScheduler_MetaData_h
