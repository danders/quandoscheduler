/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerPluginManager.h"

#include "QuandoSchedulerPluginInterface.h"
#include "QuandoSchedulerMetaData.h"
#include "DebugLib.h"

#include <QStringList>
#include <QDir>
#include <QCoreApplication>
#include <QPluginLoader>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

namespace QuandoScheduler {


PluginManager::PluginManager()
{
}

PluginManager::~PluginManager()
{
    for (const QMap<QString, QPluginLoader*> map : m_loaders) {
        for (QPluginLoader *pl : map) {
            pl->unload();
        }
    }
    for (QMap<QString, QPluginLoader*> map : m_loaders) {
        qDeleteAll(map);
    }
}

QStringList PluginManager::plugins() const
{
    QStringList lst;
    for (const QString &path : qApp->libraryPaths()) {
        if (path.endsWith("quandoscheduler")) {
            lst += plugins(path);
        } else {
            lst += plugins(path + "/quandoscheduler");
        }
    }
    return lst;
}

QStringList PluginManager::plugins(const QString &path) const
{
    QStringList result;
    QDir pluginsDir(path);
// #if defined(Q_OS_WIN)
//     if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
//         pluginsDir.cdUp();
// #elif defined(Q_OS_MAC)
//     if (pluginsDir.dirName() == "MacOS") {
//         pluginsDir.cdUp();
//         pluginsDir.cdUp();
//         pluginsDir.cdUp();
//     }
// #endif
//     pluginsDir.cd("plugins");
    debugLib<<"PluginManager::plugins:"<<pluginsDir.path();
    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        debugLib<<"PluginManager::plugins:"<<pluginsDir.path()<<fileName;
        QPluginLoader *loader = new QPluginLoader(pluginsDir.absoluteFilePath(fileName));
        QJsonObject json = loader->metaData();
        if (json.isEmpty()) {
            debugLib<<"No json object present";
            delete loader;
            continue;
        }
        QJsonObject data = json.value(QLatin1String("MetaData")).toObject();

        debugLib<<"JSON:"<<data.toVariantMap();
        
        data = data.value(QLatin1String("QuandoSchedulerPlugin")).toObject();
        if (data.isEmpty()) {
            debugLib<<"Not a QuandoSchedulerPlugin";
            delete loader;
            continue;
        }            
        const QString category = data.value(QLatin1String("Category")).toString();
        QString id = data.value(QLatin1String("Id")).toString();
        if (!id.isEmpty()) {
            result << id;
            result << data.value(QLatin1String("Name")).toString();
            result << data.value(QLatin1String("Description")).toString();
            if (!m_loaders.keys().contains(category) || !m_loaders[category].contains(id)) {
                m_loaders[category][id] = loader;
            }
        }
        if (!m_loaders.keys().contains(category) || !m_loaders[category].contains(id)) {
            delete loader;
        }
    }
    debugLib<<result;
    return result;
}

PluginInterface *PluginManager::plugin(const QString &id)
{
    if (id.isEmpty()) {
        return 0;
    }
    for (const QMap<QString, QPluginLoader*> loaders : m_loaders) {
        if (loaders.contains(id)) {
            return qobject_cast<PluginInterface*>(loaders[id]->instance());
        }
    }
    QDir pluginsDir(qApp->libraryPaths().value(0));
#if defined(Q_OS_WIN)
    if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
        pluginsDir.cdUp();
#elif defined(Q_OS_MAC)
    if (pluginsDir.dirName() == "MacOS") {
        pluginsDir.cdUp();
        pluginsDir.cdUp();
        pluginsDir.cdUp();
    }
#endif
    pluginsDir.cd("plugins");
    debugLib<<"PluginManager::plugins:"<<pluginsDir;
    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        QPluginLoader *loader = new QPluginLoader(pluginsDir.absoluteFilePath(fileName));
        QJsonObject json = loader->metaData();
        if (json.isEmpty()) {
            delete loader;
            continue;
        }
        QJsonObject data = json.value(QLatin1String("QuandoSchedulerPlugin")).toObject();
        if (!data.value(QLatin1String("Id")).toArray().contains(QJsonValue(id))) {
            delete loader;
            continue;
        }
        PluginInterface *pl = qobject_cast<QuandoScheduler::PluginInterface*>(loader->instance());
        if (pl) {
            const QString category = data.value(QLatin1String("Category")).toString();
            m_loaders[category][id] = loader;
        } else {
            delete loader;
        }
    }
    return 0;
}

QJsonObject PluginManager::jsonData(const QString &pluginId, const QString &scriptId) const
{
    QJsonObject json;
    for (const QMap<QString, QPluginLoader*> loaders : m_loaders) {
        if (loaders.contains(pluginId)) {
            json = loaders[pluginId]->metaData().value(QLatin1String("MetaData")).toObject();;
        }
    }
    if (!json.isEmpty()) {
        if ( !scriptId.isEmpty()) {
            // TODO: get script metadata
            //json.clear();
        }
    }
    return json;
}

MetaData *PluginManager::metaData(const QString &pluginId, const QString &scriptId) const
{
    MetaData *md = new MetaData();
    QJsonObject json = jsonData(pluginId, scriptId);
    if (!json.isEmpty()) {
        // here we know we have the correct plugin id
        md->setData(json.value(QLatin1String("QuandoSchedulerPlugin")).toObject());
    }
    return md;
}

} // namespace QuandoScheduler
