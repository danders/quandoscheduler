/* This file is part of the QuandoScheduler project.
   Copyright (C) 2017 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerAssignment.h"

#include "QuandoScheduler.h"

#include "QuandoSchedulerResource.h"
#include "QuandoSchedulerTask.h"

namespace QuandoScheduler {

Assignment::Assignment()
    : m_task(0)
    , m_resource(0)
{
}

Assignment::Assignment(Task *task, Resource *resource)
    : m_task(task)
    , m_resource(resource)
{
}

Assignment::Assignment(const Assignment &other)
{
    m_task = other.m_task;
    m_resource = other.m_resource;
    m_from = other.m_from;
    m_until = other.m_until;
    m_amount = other.m_amount;
}

bool Assignment::isValid() const
{
    return m_task != 0 && m_resource != 0;
}

bool Assignment::isOpen() const
{
    return m_from.count() > m_until.count();
}

void Assignment::setTask(Task *task)
{
    m_task = task;
}

Task *Assignment::task() const
{
    return m_task;
}

void Assignment::setResource(Resource *resource)
{
    m_resource = resource;
}

Resource *Assignment::resource() const
{
    return m_resource;
}

int Assignment::count() const
{
    return m_from.count();
}

void Assignment::setAmount(double amount)
{
    m_amount << amount;
}

double Assignment::amount(int pos) const
{
    int p = pos == -1 ? m_amount.count() - 1 : pos;
    return m_amount.value(p);
}

void Assignment::setFrom(const QDateTime &from)
{
    Q_ASSERT(!isOpen());
    m_from << from;
}

QDateTime Assignment::from(int pos) const
{
    return m_from.value(pos);
}

void Assignment::setUntil(const QDateTime &until)
{
    Q_ASSERT(isOpen());
    m_until << until;
}

QDateTime Assignment::until(int pos) const
{
    return m_until.value(pos);
}

} // namespace QuandoScheduler

QDebug operator<<(QDebug dbg, const QuandoScheduler::Assignment *a)
{
    dbg.nospace() << "Assignment[";
    if (!a->isValid()) {
        dbg << "Invalid";
    } else if (a->isOpen()) {
        dbg << "Open" << a->task() << " - " << a->resource();
    } else {
        if (a->task() && !a->task()->objectName().isEmpty()) {
            dbg << "Task[" << a->task()->objectName() << ']';
        } else {
            dbg << "Task[" << a->task() << ']';
        }
        dbg << " - ";
        if (a->resource() && !a->resource()->objectName().isEmpty()) {
            dbg << "Resource[" << a->resource()->objectName() << ']';
        } else {
            dbg << "Resource[" << a->resource() << ']';
        }
        for (int i = 0; i < a->count(); ++i) {
            dbg << endl << "  " << a->from(i) << '-' << a->until(i) << ':' << a->amount(i);
        }
        dbg << endl;
    }
    dbg << ']';
    return dbg.space();
}
