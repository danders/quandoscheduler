/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerDependency.h"

#include "QuandoSchedulerTask.h"
#include "QuandoSchedulerAvailable.h"

namespace QuandoScheduler {

Dependency::Dependency()
    : m_predeccessor(0)
    , m_successor(0)
    , m_type(0)
    , m_lag(0)

{
}

Dependency::Dependency(Task *predeccessor, Task *successor, int type, int lag)
    : m_predeccessor(predeccessor)
    , m_successor(successor)
    , m_type(type)
    , m_lag(lag)
{
}

Dependency::~Dependency()
{
    detach();
}

Task *Dependency::predeccessor() const
{
    return m_predeccessor;
}

Task *Dependency::successor() const
{
    return m_successor;
}

int Dependency::type() const
{
    return m_type;
}

int Dependency::lag() const
{
    return m_lag;
}

void Dependency::setLag(double duration)
{
    m_lag = duration;
}

void Dependency::addAvailability(int type, const QDateTime& from, const QDateTime& until)
{
    Available *a = new Available();
    a->type = type;
    a->from = from;
    a->until = until;
    m_availability.append(a);
}

QList<Available*> Dependency::availabilities() const
{
    return m_availability;
}


void Dependency::detach()
{
    m_predeccessor->removeSuccessor(this);
    m_successor->removePredeccessor(this);
}

void Dependency::atach()
{
    m_predeccessor->addSuccessor(this);
    m_successor->addPredeccessor(this);
}

} // namespace QuandoScheduler

QDebug operator<<(QDebug dbg, QuandoScheduler::Dependency *dep)
{
    dbg << "Dependency[";
    dbg.nospace() << (dep->predeccessor()?dep->predeccessor()->objectName():"0x0")
                  << "->"
                  << (dep->successor()?dep->successor()->objectName():"0x0");
    dbg << ']';
    return dbg;
}
