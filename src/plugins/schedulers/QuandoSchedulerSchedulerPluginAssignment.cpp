/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerSchedulerPluginAssignment.h"
#include "QuandoSchedulerSchedulerPluginTask.h"
#include "QuandoSchedulerSchedulerPluginResource.h"
#include "debug.h"

#include <QStringList>

namespace QuandoSchedulerScheduler {

Assignment resourceUnavailable(QuandoScheduler::AT_Unavailable);
Assignment resourceVacation(QuandoScheduler::AT_Vacation);
Assignment resourceTimeOff(QuandoScheduler::AT_TimeOff);
Assignment resourceAvailable(QuandoScheduler::AT_Available);

Assignment *taskUnavailable = &resourceUnavailable;
Assignment *taskAvailable = &resourceAvailable;

Assignment::Assignment(int typ)
    : type(typ)
{
}

Assignment::~Assignment()
{
}

bool Assignment::isStatic(const Assignment *a)
{
    return a == &resourceUnavailable || a == &resourceTimeOff || a == &resourceVacation || a == &resourceAvailable;
}

ResourceBooking::ResourceBooking()
    : Assignment(QuandoScheduler::AT_Booking)
{
}

double ResourceBooking::book(TaskData* taskdata, double amount)
{
    assignments[taskdata] = amount;
    totalAmount += amount;
    return amount;
}

double ResourceBooking::amount(const TaskData *taskdata) const
{
    return assignments.value(const_cast<TaskData*>(taskdata));
}

TaskAssignment::TaskAssignment()
{
}

QDebug operator<<(QDebug dbg, const Assignment *a)
{
    static QString types = "Unavailable,Vacation,TimeOff,Available,Booking";
    dbg.nospace() << " Assignment[" << types.split(',').value(a->type) << "] ";
    return dbg;
}

} // namespace QuandoSchedulerScheduler

