/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoSchedulerScheduler_Task_h
#define QuandoSchedulerScheduler_Task_h

#include "quandoschedulerschedulerplugin_export.h"

#include <QList>
#include <QDateTime>
#include <QDebug>

namespace QuandoScheduler {
    class Task;
}

namespace QuandoSchedulerScheduler {

class Assignment;
class Project;


class QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT Task
{
public:
    Task(Project *proj, QuandoScheduler::Task *task, const QDateTime &zero, const QDateTime &max, int granularity);
    Task(const Task &task);
    ~Task();

    Project *project;

    QuandoScheduler::Task *qsTask;

    int targetStart;
    int targetFinish;
    int estimate;
    QList<Assignment*>  available; // used for Length calculations

    QList<Task*> predeccessors;
    QList<int> predTypes;
    QList<Task*> successors;
    QList<int> succTypes;
};

class QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT TaskData
{
public:
    TaskData(Task *task);
    TaskData(TaskData &info);

    Task *task;

    QList<TaskData*> predeccessors;
    QList<int> predTypes;
    QList<TaskData*> successors;
    QList<int> succTypes;

    bool scheduled;
    int actualStart;
    int actualFinish;
    double actualAmount;
};


} // namespace QuandoSchedulerScheduler

QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT QDebug operator<<(QDebug dbg, QuandoSchedulerScheduler::Task *t);
QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT QDebug operator<<(QDebug dbg, QuandoSchedulerScheduler::TaskData *t);

#endif // QuandoSchedulerScheduler_Task_h
