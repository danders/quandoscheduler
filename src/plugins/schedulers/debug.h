/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoScheduler_SchedulerDebug_h
#define QuandoScheduler_SchedulerDebug_h

#include <QLoggingCategory>

extern const QLoggingCategory &SCHEDULERPLUGIN_LOG();

#define debugPlugin qCDebug(SCHEDULERPLUGIN_LOG)
#define debugPluginF qCDebug(SCHEDULERPLUGIN_LOG)<<Q_FUNC_INFO
#define warnPlugin qCWarning(SCHEDULERPLUGIN_LOG)
#define errorPlugin qCCritical(SCHEDULERPLUGIN_LOG)
    

#endif // QuandoScheduler_SchedulerDebug_h
