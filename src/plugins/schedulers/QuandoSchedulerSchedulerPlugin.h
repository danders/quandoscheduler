/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoSchedulerSchedulerPlugin_h
#define QuandoSchedulerSchedulerPlugin_h

#include "quandoschedulerschedulerplugin_export.h"

#include "QuandoSchedulerSchedulerProblem.h"

#include <QuandoSchedulerPluginInterface.h>

#include <QObject>
#include <QtPlugin>
#include <QVariantMap>

namespace QuandoScheduler {
    class Data;
    class Project;
    class Task;
    class Resource;
    class Allocation;
}

namespace QuandoSchedulerScheduler {
    class Project;
    class Task;
    class Assignment;
    class Resource;
    class Solution;
}

class QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT QuandoSchedulerSchedulerPlugin : public QObject, public QuandoScheduler::PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QuandoScheduler_PluginInterface_iid FILE "QuandoSchedulerSchedulerPlugin.json")
    Q_INTERFACES(QuandoScheduler::PluginInterface)

public:
    bool initiate(QuandoScheduler::Main *controller, QuandoScheduler::Data &data, const QVariantMap &args);
    void execute();
    int state() const;
    QStringList messages() const;
    void clear();

    QMap<double, QuandoSchedulerScheduler::Solution*> solutions() const;
    QuandoSchedulerScheduler::Solution *bestSolution() const;
    QuandoSchedulerScheduler::Solution *randomSolution(int range = -1) const;

Q_SIGNALS:
    void progress(int);
    void message(const QString &msg);

protected:
    void addMessage(const QString &msg);

    bool prepare();
    void doTaskForward(QuandoSchedulerScheduler::Task* task, QMap<QuandoScheduler::Task*, QuandoSchedulerScheduler::Task*> &taskmap);
    void doTaskBackward(QuandoSchedulerScheduler::Task* task, QMap<QuandoScheduler::Task*, QuandoSchedulerScheduler::Task*> &taskmap);

    void executeQuick();
    void executePert();
    void executeEvolution();

    int toInt(const QDateTime &time) const;
    int toEnd(const QDateTime &time) const;
    int fromDuration(double duration) const;
    QDateTime fromInt(int slot) const;
    QDateTime fromEnd(int slot) const;
    
private:
    QVariantMap m_args;
    int m_range;
    QuandoScheduler::Data *m_data;
    QList<bool> m_initiatedOk;
    QStringList m_messages;

    QuandoSchedulerScheduler::Problem problem;
    QMultiMap<double, QuandoSchedulerScheduler::Solution*> m_solutions;
};

#endif // QuandoSchedulerSchedulerPlugin_h
