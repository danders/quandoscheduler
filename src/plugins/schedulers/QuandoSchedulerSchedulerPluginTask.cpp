/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerSchedulerPluginTask.h"
#include "QuandoSchedulerSchedulerPluginAssignment.h"
#include "debug.h"

#include "QuandoScheduler.h"
#include "QuandoSchedulerAvailable.h"
#include "QuandoSchedulerTask.h"

namespace QuandoSchedulerScheduler {


Task::Task(Project *proj, QuandoScheduler::Task *task, const QDateTime &zero, const QDateTime &max, int granularity)
    : project(proj)
    , qsTask(task)
    , targetStart(0)
    , targetFinish(-1)
    , estimate(0)
{
    if (task->estimateType() == QuandoScheduler::ET_Length) {
        qint64 range = zero.secsTo(max) / granularity;
        debugPlugin<<task<<zero<<max<<granularity<<range;
        Q_ASSERT(range > 0);
        available.reserve(range);
        for (int i = 0; i < range; ++i) {
            available.append(taskUnavailable);
        }
        for (const QuandoScheduler::Available *a : task->availabilities()) {
            if (a->type == QuandoScheduler::AT_Available) {
                qint64 start = qMax((qint64)0, zero.secsTo(a->from) / granularity);
                qint64 stop = qMin(range, zero.secsTo(a->until) / granularity);
                debugPlugin<<a->from<<a->until<<start<<stop;
                for (int i = start; i < stop; ++i) {
                    available[i] = taskAvailable;
                }
            }
        }
        //debugPlugin<<available;
    }
}

Task::Task(const Task &task)
{
    project = task.project;
    qsTask = task.qsTask;
    targetStart = task.targetStart;
    targetFinish = task.targetFinish;
    available = task.available;

    succTypes = task.succTypes;
    predTypes = task.predTypes;
}

Task::~Task()
{
}

//-----------------------------------
TaskData::TaskData(Task *tsk)
: task(tsk)
, scheduled(false)
, actualStart(0)
, actualFinish(-1)
, actualAmount(0)
{
}

TaskData::TaskData(TaskData &data)
{
    task = data.task;
    scheduled = data.scheduled;
    actualStart = data.actualFinish;
    actualFinish = data.actualAmount;
    actualAmount = data.actualAmount;
}

} // namespace QuandoSchedulerScheduler


QDebug operator<<(QDebug dbg, QuandoSchedulerScheduler::Task *t)
{
    dbg << "Task[";
    dbg.nospace() << (void*)t;
    if (t && t->qsTask) {
        dbg.nospace() << ", " << t->qsTask->objectName();
    }
    dbg << ']';
    return dbg.space();
}

QDebug operator<<(QDebug dbg, QuandoSchedulerScheduler::TaskData *t)
{
    dbg << "TaskData[";
    dbg.nospace() << (void*)t;
    if (t && t->task && t->task->qsTask) {
        dbg.nospace() << ", " << t->task->qsTask->objectName();
    }
    dbg << ']';
    return dbg.space();
}
