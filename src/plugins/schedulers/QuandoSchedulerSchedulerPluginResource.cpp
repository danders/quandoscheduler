/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerSchedulerPluginResource.h"
#include "QuandoSchedulerSchedulerPluginAssignment.h"
#include "debug.h"

#include <QuandoSchedulerAllocation.h>
#include <QuandoSchedulerAvailable.h>
#include <QuandoSchedulerResource.h>

namespace QuandoSchedulerScheduler {


Resource::Resource(const QuandoScheduler::Allocation *allocation, const QDateTime &zero, const QDateTime &max, int granularity)
    : qsResource(allocation->resource())
{
    amountPrSlot = qsResource->amount() * granularity / 3600;
    if (assignments.isEmpty()) {
        qint64 range = zero.secsTo(max) / granularity;
        debugPlugin<<qsResource<<zero<<max<<granularity<<range;
        Q_ASSERT(range > 0);
        assignments.reserve(range);
        for (int i = 0; i < range; ++i) {
            assignments << &resourceUnavailable;
        }
        for (const QuandoScheduler::Available *a : qsResource->availabilities()) {
            qint64 start = qMax((qint64)0, zero.secsTo(a->from) / granularity);
            qint64 stop = qMin(range, zero.secsTo(a->until) / granularity);
            debugPlugin<<qsResource<<a->from<<a->until<<start<<stop;
            for (int i = start; i < stop; ++i) {
                Assignment *as = &resourceUnavailable;
                if (a->type == QuandoScheduler::AT_Available) {
                    if (a->amount > 0.0) {
                        as = &resourceAvailable;
                    }
                } else if (a->type == QuandoScheduler::AT_Vacation) {
                    as = &resourceVacation;
                } else if (a->type == QuandoScheduler::AT_TimeOff) {
                    as = &resourceTimeOff;
                }
                assignments[i] = as;
            }
        }
        //debugPlugin<<assignments;
    }
}

Resource::~Resource()
{
    for (int i = 0; i < assignments.count(); ++i) {
        Assignment *a = assignments.at(i);
        if (!Assignment::isStatic(a)) {
            delete a;
        }
    }
}

//TODO
bool Resource::allowOverload() const
{
    return true;
}

} // namespace QuandoSchedulerScheduler

QDebug operator<<(QDebug dbg, QuandoSchedulerScheduler::Resource *r)
{
    dbg << "Resource[";
    dbg.nospace() << (void*)r;
    if (r) {
        dbg.nospace() << ", " << r->qsResource->objectName();
    }
    dbg << ']';
    return dbg.space();
}
