/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoSchedulerScheduler_Assignment_h
#define QuandoSchedulerScheduler_Assignment_h

#include "quandoschedulerschedulerplugin_export.h"

#include <QuandoScheduler.h>

#include <QList>
#include <QMap>
#include <QDebug>

namespace QuandoSchedulerScheduler {

class TaskData;
class Resource;

class QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT Assignment
{
public:    
    Assignment(int type = QuandoScheduler::AT_Unavailable);
    ~Assignment();

    int type;

    static bool isStatic(const Assignment *a);
};

extern Assignment resourceUnavailable;
extern Assignment resourceVacation;
extern Assignment resourceTimeOff;
extern Assignment resourceAvailable;

extern Assignment *taskUnavailable;
extern Assignment *taskAvailable;

class ResourceBooking : public Assignment
{
public:
    ResourceBooking();

    double book(TaskData *taskdata, double amount);
    double amount(const TaskData *taskdata) const;

    double totalAmount; // for this assignment
    QMap<TaskData*, double> assignments;
};

class TaskAssignment
{
public:
    TaskAssignment();
    
    QMap<Resource*, double> assignments;
};

QDebug operator<<(QDebug dbg, const Assignment *a);

} // namespace QuandoSchedulerScheduler

#endif // QuandoSchedulerScheduler_Assignment_h
