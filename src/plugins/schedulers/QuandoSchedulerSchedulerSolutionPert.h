/* This file is part of the QuandoScheduler project.
   Copyright (C) 2017 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoSchedulerScheduler_SolutionPert_h
#define QuandoSchedulerScheduler_SolutionPert_h

#include "quandoschedulerschedulerplugin_export.h"

#include "QuandoSchedulerSchedulerSolution.h"


namespace QuandoScheduler {
    class Resource;
}

namespace QuandoSchedulerScheduler {

class Project;
class Task;
class TaskData;

class QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT SolutionPert : public Solution
{
    Q_OBJECT
public:
    SolutionPert();
    SolutionPert(Problem &problem);
    SolutionPert(const Solution &solution);
    ~SolutionPert();

    void solve() override;

    void operator=(const SolutionPert &solution);

protected:
    void schedule(TaskData *taskdata);
    void scheduleEffortForward(TaskData *taskdata);
    void scheduleDurationForward(TaskData *taskdata);
    void scheduleLengthForward(TaskData *taskdata);
    void scheduleEffortBackward(TaskData *taskdata);
    void scheduleDurationBackward(TaskData *taskdata);
    void scheduleLengthBackward(TaskData *taskdata);    
};

} // namespace QuandoScheduler

#endif // QuandoSchedulerScheduler_SolutionPert_h
