/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerSchedulerSolution.h"
#include "QuandoSchedulerSchedulerPluginProject.h"
#include "QuandoSchedulerSchedulerPluginTask.h"
#include "QuandoSchedulerSchedulerPluginResource.h"
#include "QuandoSchedulerSchedulerPluginAssignment.h"
#include "debug.h"

#include <QuandoScheduler.h>
#include <QuandoSchedulerTask.h>
#include <QuandoSchedulerProject.h>
#include <QuandoSchedulerResource.h>
#include <QuandoSchedulerAllocation.h>

namespace QuandoSchedulerScheduler {

Solution::Solution()
    : m_fitness(0.0)
    , problem(emptyProblem)
{
}

Solution::Solution(Problem &problem)
    : problem(problem)
{
    debugPlugin<<problem;
    for (Task *t : problem.tasks) {
        taskSequence.append(new TaskData(t));
    }
    for (Task *t : problem.startTasks) {
        startTasks.append(new TaskData(t));
    }
    for (Task *t : problem.endTasks) {
        endTasks.append(new TaskData(t));
    }
    // dependencies
    for (TaskData *taskdata : taskSequence) {
        debugPlugin<<taskdata<<taskdata->task;
        for (int i = 0; i < taskdata->task->successors.count(); ++i) {
            Task *succ = taskdata->task->successors.at(i);
            int type = taskdata->task->succTypes.at(i);
            TaskData *s = findTaskData(succ);
            debugPlugin<<"succs:"<<taskdata<<"->"<<succ<<type<<s;
            Q_ASSERT(s);
            taskdata->successors << s;
            taskdata->succTypes << type;
        }
        for (Task *pred : taskdata->task->predeccessors) {
            for (int i = 0; i < taskdata->task->predeccessors.count(); ++i) {
                Task *pred = taskdata->task->predeccessors.at(i);
                int type = taskdata->task->predTypes.at(i);
                TaskData *p = findTaskData(pred);
                debugPlugin<<"preds:"<<taskdata<<"<-"<<pred<<type<<p;
                Q_ASSERT(p);
                taskdata->predeccessors << p;
                taskdata->predTypes << type;
            }
        }
    }
    for (TaskData *taskdata : startTasks) {
        for (int i = 0; i < taskdata->task->successors.count(); ++i) {
            Task *succ = taskdata->task->successors.at(i);
            int type = taskdata->task->succTypes.at(i);
            TaskData *s = findTaskData(succ);
            debugPlugin<<"start:"<<taskdata<<"->"<<succ<<type<<s;
            Q_ASSERT(s);
            taskdata->successors << s;
            taskdata->succTypes << type;
        }
    }
    for (TaskData *taskdata : endTasks) {
        for (int i = 0; i < taskdata->task->predeccessors.count(); ++i) {
            Task *pred = taskdata->task->predeccessors.at(i);
            int type = taskdata->task->predTypes.at(i);
            TaskData *p = findTaskData(pred);
            debugPlugin<<"end:"<<taskdata<<"<-"<<pred<<type<<p;
            Q_ASSERT(p);
            taskdata->predeccessors << p;
            taskdata->predTypes << type;
        }
    }
    debugPlugin<<this;
}

Solution::~Solution()
{
    qDeleteAll(taskSequence);
    qDeleteAll(startTasks);
    qDeleteAll(endTasks);
}

void Solution::operator=(const Solution &solution)
{
    copy(solution);
}

void Solution::copy(const Solution &solution)
{
    clear();
    m_fitness = solution.m_fitness;
    problem = solution.problem;

    for (TaskData *i : solution.taskSequence) {
        taskSequence << new TaskData(*i);
    }
    for (TaskData *i : solution.startTasks) {
        startTasks << new TaskData(*i);
    }
    for (TaskData *i : solution.endTasks) {
        endTasks << new TaskData(*i);
    }
}

bool Solution::isEmpty() const
{
    return problem.isEmpty();
}

void Solution::clear()
{
    m_fitness = 0.0;
    problem = emptyProblem;

    qDeleteAll(taskSequence);
    taskSequence.clear();
    qDeleteAll(startTasks);
    startTasks.clear();
    qDeleteAll(endTasks);
    endTasks.clear();
}

double Solution::fitness() const
{
    return m_fitness;
}

void Solution::addMessage(const QString &msg)
{
    emit sendMessage(msg);
}


double Solution::bookResource(TaskData *taskdata, Resource* resource, const QuandoScheduler::Allocation *allocation, int slot)
{
    double amount = 0.0;
    Assignment *a = resource->assignments.at(slot);
    debugPluginF<<taskdata<<resource<<"slot:"<<slot<<"allocation:"<<allocation<<"ass:"<<a;
    switch (a->type) {
        case QuandoScheduler::AT_Available:
            a = new ResourceBooking();
            resource->assignments[slot] = a;
            // fall through
        case QuandoScheduler::AT_Booking: {
            ResourceBooking *b = static_cast<ResourceBooking*>(a);
            Q_ASSERT(!b->assignments.contains(taskdata));
            double req = resource->amountPrSlot * allocation->amount();
            debugPlugin<<resource<<"pr slot:"<<resource->amountPrSlot<<"aloc'ed:"<<allocation->amount()<<"req:"<<req;
            if (b->totalAmount + req <= resource->amountPrSlot || resource->allowOverload()) {
                amount = b->book(taskdata, req);
            }
            break;
        }
        default:
            break;
    }
    debugPluginF<<taskdata<<resource<<"slot:"<<slot<<"amount:"<<amount;
    return amount;
}

Resource *Solution::resource(const QuandoScheduler::Allocation* a)
{
    Resource *r = 0;
    if (resources.contains(a->resource())) {
        r = resources[a->resource()];
    } else {
        r = new Resource(a, problem.zeroTime, problem.maxTime, problem.granularity);
        resources[a->resource()] = r;
    }
    return r;
}

TaskData *Solution::findTaskData(Task *task) const
{
    for (TaskData *i : taskSequence) {
        if (i->task == task) {
            return i;
        }
    }
    for (TaskData *i : startTasks) {
        if (i->task == task) {
            return i;
        }
    }
    for (TaskData *i : endTasks) {
        if (i->task == task) {
            return i;
        }
    }
    return 0;
}

} // namespace QuandoSchedulerScheduler

QDebug operator<<(QDebug dbg, const QuandoSchedulerScheduler::Solution *s)
{
    if (s) {
        return dbg << *s;
    }
    return dbg << "Solution[0x0]";
}

QDebug operator<<(QDebug dbg, const QuandoSchedulerScheduler::Solution &s)
{
    dbg.nospace() << "Solution[" << endl;
    for (QuandoSchedulerScheduler::TaskData *t : s.startTasks) {
        dbg << '\t' << t << " preds:" << t->predeccessors << " succs:" << t->successors << endl;
    }
    for (QuandoSchedulerScheduler::TaskData *t : s.taskSequence) {
        dbg << '\t' << t << " preds:" << t->predeccessors << " succs:" << t->successors << endl;
    }
    for (QuandoSchedulerScheduler::TaskData *t : s.endTasks) {
        dbg << '\t' << t << " preds:" << t->predeccessors << " succs:" << t->successors << endl;
    }
    dbg << ']';
    return dbg.space();
}
