/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoSchedulerSchedulerPlugin_Project_h
#define QuandoSchedulerSchedulerPlugin_Project_h

#include "quandoschedulerschedulerplugin_export.h"

#include <QList>
#include <QMap>
#include <QDebug>

namespace QuandoScheduler {
    class Project;
}

namespace QuandoSchedulerScheduler {

class Task;
class Resource;

class QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT Project
{
public:
    Project(QuandoScheduler::Project *project);
    ~Project();

    QuandoScheduler::Project *qsProject;
    
    int targetStart;
    int targetFinish;

    int actualStart;
    int actualFinish;
};

} // namespace QuandoSchedulerScheduler

QDebug operator<<(QDebug dbg, const QuandoSchedulerScheduler::Project *p);

#endif // QuandoSchedulerSchedulerPlugin_Project_h
