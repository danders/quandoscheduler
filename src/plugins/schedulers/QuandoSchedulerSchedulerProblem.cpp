/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerSchedulerProblem.h"
#include "QuandoSchedulerSchedulerPluginProject.h"
#include "QuandoSchedulerSchedulerPluginTask.h"
#include "debug.h"

#include <QuandoScheduler.h>
#include <QuandoSchedulerTask.h>

namespace QuandoSchedulerScheduler {

Problem::Problem()
    : granularity(60)
{
}

Problem::~Problem()
{
}

bool Problem::isEmpty() const
{
    return projects.isEmpty();
}

void Problem::clear()
{
    granularity = 60;
    qDeleteAll(projects);
    projects.clear();
    qDeleteAll(tasks);
    tasks.clear();
    qDeleteAll(startTasks);
    startTasks.clear();
    qDeleteAll(endTasks);
    endTasks.clear();
//     dependencies.clear();
//     allocations.clear();
}

Task *Problem::findTask(QuandoScheduler::Task *qsTask) const
{
    for (Task *t : tasks) {
        if (t->qsTask == qsTask) {
            return t;
        }
    }
    for (Task *t : startTasks) {
        if (t->qsTask == qsTask) {
            return t;
        }
    }
    for (Task *t : endTasks) {
        if (t->qsTask == qsTask) {
            return t;
        }
    }
    return 0;
}


} // namespace QuandoSchedulerScheduler

QDebug operator<<(QDebug dbg, const QuandoSchedulerScheduler::Problem *p)
{
    if (p) {
        return operator<<(dbg, *p);
    }
    dbg << "Problem(0x0)";
    return dbg;
}

QDebug operator<<(QDebug dbg, const QuandoSchedulerScheduler::Problem &p)
{
    dbg.nospace() << "Problem[";
    if (p.isEmpty()) {
        dbg << "Empty";
    } else {
        dbg << "Projects:" << p.projects.count() << " Tasks:" << p.tasks.count() << endl
        << "\tProjects   : " << p.projects << endl
        << "\tStart tasks: " << p.startTasks << endl
        << "\tTasks      : " << p.tasks << endl
        << "\tEnd tasks  : " << p.endTasks << endl;
    }
    dbg << ']';
    return dbg.space();
}
