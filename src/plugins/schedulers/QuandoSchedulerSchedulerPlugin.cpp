/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerSchedulerPlugin.h"
#include "QuandoSchedulerSchedulerPluginProject.h"
#include "QuandoSchedulerSchedulerPluginAssignment.h"
#include "QuandoSchedulerSchedulerPluginResource.h"
#include "QuandoSchedulerSchedulerPluginTask.h"
#include "QuandoSchedulerSchedulerSolutionQuick.h"
#include "QuandoSchedulerSchedulerSolutionPert.h"
#include "QuandoSchedulerSchedulerSolutionEvolve.h"
#include "debug.h"

#include <QuandoScheduler.h>
#include <QuandoSchedulerData.h>
#include <QuandoSchedulerMain.h>
#include <QuandoSchedulerProject.h>
#include <QuandoSchedulerAllocation.h>
#include <QuandoSchedulerDependency.h>
#include <QuandoSchedulerResource.h>
#include <QuandoSchedulerTask.h>
#include <QuandoSchedulerAssignment.h>

#include <QList>
#include <QTimer>
#include <QThread>

using namespace QuandoSchedulerScheduler;

bool QuandoSchedulerSchedulerPlugin::initiate(QuandoScheduler::Main *controller, QuandoScheduler::Data &data, const QVariantMap &args)
{
    debugPlugin<<args;
    clear();

    m_args = args;
    m_data = &data;
    m_range = toInt(data.maxTime());

    //TODO check args and data

    if (controller) {
        connect(this, SIGNAL(progress(int)), controller, SLOT(slotJobProgress(int)));
        connect(this, SIGNAL(message(QString)), controller, SLOT(slotJobMessage(QString)));
    }
    m_initiatedOk << prepare();
    return m_initiatedOk.value(0);
}

bool QuandoSchedulerSchedulerPlugin::prepare()
{
    debugPlugin<<m_data->projects();
    bool result = !m_data->projects().isEmpty();
    if (!result) {
        return result;
    }
    problem.zeroTime = m_data->zeroTime();
    problem.maxTime = m_data->maxTime();
    problem.granularity = m_data->granularity();
    QMultiMap<int, Project*> projects;
    for (QuandoScheduler::Project *qsProject : m_data->projects()) {
        Project *project = new Project(qsProject);
        project->targetStart = toInt(qsProject->targetStart());
        project->targetFinish = toEnd(qsProject->targetFinish());
        projects.insert(qsProject->priority(), project);
        result = project->targetFinish > project->targetStart;
        if (!result) {
            debugPlugin<<project<<project->targetStart<<project->targetFinish;
            break;
        }
    }
    if (!result) {
        qDeleteAll(projects);
        return result;
    }
    problem.projects = projects.values().toVector();
    QMap<QuandoScheduler::Task*, Task*> taskmap;
    for (Project *project : problem.projects) {
        Task *startTask = new Task(project, project->qsProject->startTask(), m_data->zeroTime(), m_data->maxTime(), m_data->granularity());
        startTask->targetStart = toInt(startTask->qsTask->targetStart());
        startTask->targetFinish = startTask->targetStart - 1;
        problem.startTasks.append(startTask);
        taskmap[startTask->qsTask] = startTask;

        Task *endTask = new Task(project, project->qsProject->endTask(), m_data->zeroTime(), m_data->maxTime(), m_data->granularity());
        endTask->targetFinish = toEnd(endTask->qsTask->targetFinish());
        endTask->targetStart = endTask->targetFinish + 1;
        problem.endTasks.append(endTask);
        taskmap[endTask->qsTask] = endTask;

        for (QuandoScheduler::Task *qsTask : project->qsProject->tasks()) {
            debugPlugin<<"treat:"<<qsTask;
            Task *task = new Task(project, qsTask, m_data->zeroTime(), m_data->maxTime(), m_data->granularity());
            task->targetStart = toInt(qsTask->targetStart());
            task->targetFinish = toEnd(qsTask->targetFinish());
            task->estimate = fromDuration(qsTask->estimate());
            taskmap[qsTask] = task;
            if (qsTask->successors().isEmpty()) {
                QuandoScheduler::Dependency *dep = new QuandoScheduler::Dependency(qsTask, endTask->qsTask);
                qsTask->addSuccessor(dep);
                endTask->qsTask->addPredeccessor(dep);
            }
            if (qsTask->predeccessors().isEmpty()) {
                QuandoScheduler::Dependency *dep = new QuandoScheduler::Dependency(startTask->qsTask, qsTask);
                qsTask->addPredeccessor(dep);
                startTask->qsTask->addSuccessor(dep);
            }
        }
    }
    // Add all tasks sorted by dependencies
    for (int i = 0; i < problem.projects.count(); ++i) {
        if (problem.projects.at(i)->qsProject->direction() == QuandoScheduler::Forward) {
            doTaskForward(problem.endTasks.at(i), taskmap);
        } else {
            doTaskBackward(problem.startTasks.at(i), taskmap);
        }
    }
    return result;
}

void QuandoSchedulerSchedulerPlugin::doTaskForward(Task* task, QMap<QuandoScheduler::Task*, Task*> &taskmap)
{
    debugPlugin<<"forward:"<<task<<task->qsTask->predeccessors()<<taskmap;
    for (const QuandoScheduler::Dependency *d : task->qsTask->predeccessors()) {
        Task *pred = taskmap.value(d->predeccessor());
        Q_ASSERT(pred);
        task->predeccessors.append(pred);
        task->predTypes.append(d->type());
        Q_ASSERT(task->predeccessors.count() == task->predTypes.count());
        pred->successors.append(task);
        pred->succTypes.append(d->type());
        Q_ASSERT(pred->successors.count() == pred->succTypes.count());
        if (pred) {
            doTaskForward(pred, taskmap);
        }
    }
    // add task
    if (!problem.tasks.contains(task) && !problem.startTasks.contains(task) && !problem.endTasks.contains(task)) {
        debugPlugin<<"add to problem:"<<task<<problem.tasks;
        problem.tasks.append(task);
    }
}

void QuandoSchedulerSchedulerPlugin::doTaskBackward(Task* task, QMap<QuandoScheduler::Task*, Task*> &taskmap)
{
    for (const QuandoScheduler::Dependency *d : task->qsTask->successors()) {
        Task *succ = taskmap.value(d->successor());
        task->successors.append(succ);
        succ->predeccessors.append(task);
        if (succ) {
            doTaskBackward(succ, taskmap);
        }
    }
    // add task
    problem.tasks.append(task);
    taskmap.remove(task->qsTask);
}

void QuandoSchedulerSchedulerPlugin::execute()
{
    if (m_initiatedOk.isEmpty()) {
        addMessage("Not initated");
        return;
    }
    if (!m_initiatedOk.at(0)) {
        addMessage("Initiation failed");
        return;
    }
    debugPlugin<<m_args;
    addMessage("Started");
    emit progress(0);
    if (m_args.value("scheduling") == "quick") {
        addMessage("Schedule quick");
        executeQuick();
    } else if (m_args.value("scheduling") == "evolution") {
        addMessage("Schedule evolution");
        executeEvolution();
    } else {
        addMessage("Schedule pert");
        executePert(); // default
    }
    addMessage("Cleanup");
    Solution *s = bestSolution();
    if (s) {
        for (TaskData *data : s->startTasks) {
            data->task->project->qsProject->setActualStart(fromInt(data->actualStart));
        }
        for (TaskData *data : s->endTasks) {
            data->task->project->qsProject->setActualFinish(fromEnd(data->actualFinish));
        }
        for (TaskData *data : s->taskSequence) {
            data->task->qsTask->setActualStart(fromInt(data->actualStart));
            data->task->qsTask->setActualFinish(fromEnd(data->actualFinish));
            debugPlugin<<data->task->qsTask<<data->actualStart<<data->actualFinish;
        }
        // resource assignments
        for (Resource *r : s->resources) {
            QMap<TaskData*, QuandoScheduler::Assignment*> assignments;
            QList<TaskData*> opentasks;
            for (int i = 0; i < r->assignments.count(); ++i) {
                const Assignment *a = r->assignments.at(i);
                if (a->type == QuandoScheduler::AT_Booking) {
                    const ResourceBooking *b = static_cast<const ResourceBooking*>(a);
                    // check if any assignments shall be closed
                    for (TaskData *t : opentasks) {
                        if (!b->assignments.keys().contains(t)) {
                            assignments[t]->setUntil(fromInt(i));
                        }
                    }
                    opentasks = b->assignments.keys();
                    for (TaskData *t : opentasks) {
                        Q_ASSERT(b->amount(t) > 0.0);
                        QuandoScheduler::Assignment *assignment = assignments.value(t);
                        if (!assignment) {
                            assignment = new QuandoScheduler::Assignment(t->task->qsTask, r->qsResource);
                            assignments.insert(t, assignment);
                        }
                        if (assignment->isOpen() && assignment->amount() != b->amount(t)) {
                            // close this interval
                            debugPlugin<<"close at:"<<i<< "amount change:"<<assignment->amount()<<b->amount(t);
                            assignment->setUntil(fromInt(i));
                        }
                        if (!assignment->isOpen()) {
                            assignment->setFrom(fromInt(i));
                            assignment->setAmount(b->amount(t));
                            debugPlugin<<"open:"<<assignment->amount();
                        }
                    }
                } else {
                    // close the open tasks
                    for (TaskData *t : opentasks) {
                        Q_ASSERT(assignments.contains(t));
                        if (assignments[t]->isOpen()) {
                            assignments[t]->setUntil(fromInt(i+1));
                            debugPlugin<<"close"<<t<<"at:"<<i<<assignments[t];
                        }
                    }
                }
            }
            if (!assignments.isEmpty()) {
                // close any open assignments, then commit
                for (QuandoScheduler::Assignment *a : assignments) {
                    if (a->isOpen()) {
                        a->setFrom(fromInt(r->assignments.count()));
                        debugPlugin<<"close"<<a->task()<<"at:"<<r->assignments.count();
                    }
                }
                m_data->addAssignments(assignments.values());
            }
        }
    } else {
        addMessage("No solution found");
    }
    addMessage("Finished");
    emit progress(100);
}

int QuandoSchedulerSchedulerPlugin::state() const
{
    debugPlugin;
    return 0;
}

void QuandoSchedulerSchedulerPlugin::addMessage(const QString &msg)
{
//     debugPlugin;
    m_messages << msg;
    emit message(msg);
}

QStringList QuandoSchedulerSchedulerPlugin::messages() const
{
    return m_messages;
}

void QuandoSchedulerSchedulerPlugin::clear()
{
    m_initiatedOk.clear();
    m_messages.clear();
    qDeleteAll(m_solutions);
    m_solutions.clear();
    problem.clear();
}

QMap<double, QuandoSchedulerScheduler::Solution*> QuandoSchedulerSchedulerPlugin::solutions() const
{
    return m_solutions;
}

QuandoSchedulerScheduler::Solution *QuandoSchedulerSchedulerPlugin::bestSolution() const
{
    return m_solutions.values().value(0);
}

QuandoSchedulerScheduler::Solution *QuandoSchedulerSchedulerPlugin::randomSolution(int range) const
{
    Q_UNUSED(range);
    return m_solutions.values().value(0); // TODO
}


void QuandoSchedulerSchedulerPlugin::executeQuick()
{
    SolutionQuick *s = new SolutionQuick(problem);
    s->solve();
    m_solutions.insert(s->fitness(), s);
}

void QuandoSchedulerSchedulerPlugin::executePert()
{
    SolutionPert *s = new SolutionPert(problem);
    s->solve();
    m_solutions.insert(s->fitness(), s);
}

void QuandoSchedulerSchedulerPlugin::executeEvolution()
{
    for (int i = 0; i < 600; ++i) {
        SolutionEvolve *s = new SolutionEvolve(problem);
        s->randomize();
        s->solve();
        m_solutions.insert(s->fitness(), s);
    }
    
}

int QuandoSchedulerSchedulerPlugin::toInt(const QDateTime &time) const
{
    int result = 0;
    if (time.isValid()) {
        result = m_data->zeroTime().secsTo(time) / m_data->granularity();
    }
    return result;
}

int QuandoSchedulerSchedulerPlugin::toEnd(const QDateTime &time) const
{
    return toInt(time) - 1;
}

int QuandoSchedulerSchedulerPlugin::fromDuration(double duration) const
{
    debugPlugin<<duration<<m_data->granularity()<<'='<<duration * 3600 / m_data->granularity();
    return duration * 3600 / m_data->granularity();
}

QDateTime QuandoSchedulerSchedulerPlugin::fromInt(int slot) const
{
    return m_data->zeroTime().addSecs(slot * m_data->granularity());
}

QDateTime QuandoSchedulerSchedulerPlugin::fromEnd(int slot) const
{
    return fromInt(slot + 1);
}

