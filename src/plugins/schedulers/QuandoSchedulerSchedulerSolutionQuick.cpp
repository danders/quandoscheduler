/* This file is part of the QuandoScheduler project.
   Copyright (C) 2017 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "QuandoSchedulerSchedulerSolutionQuick.h"
#include "QuandoSchedulerSchedulerPluginProject.h"
#include "QuandoSchedulerSchedulerPluginTask.h"
#include "QuandoSchedulerSchedulerPluginResource.h"
#include "QuandoSchedulerSchedulerPluginAssignment.h"
#include "debug.h"

#include <QuandoScheduler.h>
#include <QuandoSchedulerTask.h>
#include <QuandoSchedulerProject.h>
#include <QuandoSchedulerResource.h>
#include <QuandoSchedulerAllocation.h>

namespace QuandoSchedulerScheduler {

SolutionQuick::SolutionQuick()
    : Solution()
{
}

SolutionQuick::SolutionQuick(Problem &problem)
    : Solution(problem)
{
}

SolutionQuick::~SolutionQuick()
{
}

void SolutionQuick::operator=(const SolutionQuick &solution)
{
    copy(solution);
}

void SolutionQuick::solve()
{
    QStringList lst;
    for (TaskData *taskdata : startTasks + taskSequence + endTasks) {
        lst << taskdata->task->qsTask->objectName();
    }
    debugPlugin<<"solve:"<<lst;
    for (TaskData *taskdata : startTasks + taskSequence + endTasks) {
        schedule(taskdata);
    }
}

void SolutionQuick::schedule(TaskData* taskdata)
{
    debugPlugin<<"schedule:"<<taskdata<<taskdata->predeccessors<<taskdata->predTypes;
    taskdata->actualStart = taskdata->task->targetStart;
    taskdata->actualFinish = taskdata->task->targetFinish;
    // find start/finish according to succs/preds
    if (taskdata->task->project->qsProject->direction() == QuandoScheduler::Forward) {
        for (int i = 0; i < taskdata->task->predeccessors.count(); ++i) {
            TaskData *pred = taskdata->predeccessors.at(i);
            switch (taskdata->predTypes.at(i)) {
                case QuandoScheduler::DT_FinishStart:
                    taskdata->actualStart = qMax(taskdata->actualStart, pred->actualFinish + 1);
                    taskdata->actualFinish = qMax(taskdata->actualFinish, taskdata->actualStart - 1);
                    break;
                case QuandoScheduler::DT_StartStart:
                    taskdata->actualStart = qMax(taskdata->actualStart, pred->actualStart);
                    taskdata->actualFinish = qMax(taskdata->actualFinish, taskdata->actualStart - 1);
                    break;
                case QuandoScheduler::DT_FinishFinish:
                    taskdata->actualFinish = qMax(taskdata->actualFinish, pred->actualFinish);
                    taskdata->actualStart = qMin(taskdata->actualStart, taskdata->actualFinish + 1);
                    break;
                default:
                    taskdata->actualStart = qMax(taskdata->actualStart, pred->actualFinish + 1);
                    taskdata->actualFinish = qMax(taskdata->actualFinish, taskdata->actualStart - 1);
                    break;
            }
        }
        debugPlugin<<"Schedule task forward:"<<taskdata->task<<taskdata->actualStart<<taskdata->actualFinish;
        switch (taskdata->task->qsTask->estimateType()) {
            case QuandoScheduler::ET_Effort:
                scheduleEffortForward(taskdata);
                break;
            case QuandoScheduler::ET_Duration:
                scheduleDurationForward(taskdata);
                break;
            case QuandoScheduler::ET_Length:
                scheduleLengthForward(taskdata);
                break;
            default:
                debugPlugin<<"Unknown estimate type";
                break;
        }
    } else {
        for (int i = 0; i < taskdata->successors.count(); ++i) {
            TaskData *succ = taskdata->successors.at(i);
            switch (taskdata->succTypes.at(i)) {
                case QuandoScheduler::DT_FinishStart:
                    taskdata->actualFinish = qMin(taskdata->actualFinish, succ->actualStart - 1);
                    taskdata->actualStart = qMin(taskdata->actualStart, succ->actualStart);
                    break;
                case QuandoScheduler::DT_StartStart:
                    taskdata->actualStart = qMin(taskdata->actualStart, succ->actualStart);
                    taskdata->actualFinish = qMin(taskdata->actualFinish, taskdata->actualStart - 1);
                    break;
                case QuandoScheduler::DT_FinishFinish:
                    taskdata->actualFinish = qMin(taskdata->actualFinish, succ->actualFinish);
                    taskdata->actualStart = qMin(taskdata->actualStart, taskdata->actualFinish + 1);
                    break;
                default:
                    taskdata->actualFinish = qMin(taskdata->actualFinish, succ->actualFinish);
                    taskdata->actualStart = qMin(taskdata->actualStart, succ->actualStart);
                    break;
            }
        }
        debugPlugin<<"Schedule task backward:"<<taskdata->task<<taskdata->actualStart<<taskdata->actualFinish;
        switch (taskdata->task->qsTask->estimateType()) {
            case QuandoScheduler::ET_Effort:
                scheduleEffortBackward(taskdata);
                break;
            case QuandoScheduler::ET_Duration:
                scheduleDurationBackward(taskdata);
                break;
            case QuandoScheduler::ET_Length:
                scheduleLengthBackward(taskdata);
                break;
            default:
                debugPlugin<<"Unknown estimate type";
                break;
        }
    }
    if (!taskdata->scheduled) {
        //TODO Handle error
        addMessage(QString("Failed to schedule %1").arg(taskdata->task->qsTask->objectName()));
    }
}

void SolutionQuick::scheduleEffortForward(TaskData *taskdata)
{
    int estimate = taskdata->task->estimate;
    if (estimate == 0) {
        taskdata->scheduled = true;
        taskdata->actualFinish = taskdata->actualStart - 1;
        return;
    }
    int limit = taskdata->task->project->targetFinish;
    for (int i = taskdata->actualStart; !taskdata->scheduled && i <= limit; ++i) {
        for (const QuandoScheduler::Allocation *a : taskdata->task->qsTask->allocations()) {
            Resource *r = resource(a);
            taskdata->actualAmount += bookResource(taskdata, r, a, i);
            debugPlugin<<taskdata->task<<i<<taskdata->actualAmount<<estimate;
            if (taskdata->actualAmount >= estimate) {
                taskdata->scheduled = true;
                taskdata->actualFinish = i;
                break;
            }
        }
    }
}

void SolutionQuick::scheduleDurationForward(TaskData *taskdata)
{
    int duration = taskdata->task->estimate;
    taskdata->actualFinish = taskdata->actualStart + duration - 1;
    for (int i = taskdata->actualStart; i < taskdata->actualFinish; ++i) {
        for (const QuandoScheduler::Allocation *a : taskdata->task->qsTask->allocations()) {
            Resource *r = resource(a);
            taskdata->actualAmount += bookResource(taskdata, r, a, i);
        }
    }
    taskdata->scheduled = true;
    debugPlugin<<"duration forward:"<<taskdata->task<<taskdata->actualStart<<taskdata->actualFinish;
}

void SolutionQuick::scheduleLengthForward(TaskData *taskdata)
{
    int limit = qMin(taskdata->task->available.count(), taskdata->task->project->targetFinish + 1);
    int length = taskdata->task->estimate;
    int end = 0;
    while (taskdata->actualStart < limit && taskdata->task->available.at(taskdata->actualStart)->type != QuandoScheduler::AT_Available) {
        ++taskdata->actualStart;
    }
    for (int i = taskdata->actualStart; end < length && i < limit; ++i, ++end) {
        if (taskdata->task->available.at(i)->type == QuandoScheduler::AT_Available) {
            for (const QuandoScheduler::Allocation *a : taskdata->task->qsTask->allocations()) {
                Resource *r = resource(a);
                taskdata->actualAmount += bookResource(taskdata, r, a, i);
            }
        }
    }
    taskdata->scheduled = true;
    taskdata->actualFinish = taskdata->actualStart + end - 1;
    debugPlugin<<"length:"<<taskdata<<taskdata->task->estimate<<taskdata->actualStart<<taskdata->actualFinish<<limit;
}

void SolutionQuick::scheduleEffortBackward(TaskData *taskdata)
{
    
}

void SolutionQuick::scheduleDurationBackward(TaskData *taskdata)
{
    
}

void SolutionQuick::scheduleLengthBackward(TaskData *taskdata)
{
    
}


} // namespace QuandoSchedulerScheduler
