/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoSchedulerScheduler_Solution_h
#define QuandoSchedulerScheduler_Solution_h

#include "quandoschedulerschedulerplugin_export.h"

#include "QuandoSchedulerSchedulerProblem.h"

#include <QObject>
#include <QMultiMap>
#include <QDebug>

namespace QuandoScheduler {
    class Resource;
}

namespace QuandoSchedulerScheduler {

class Project;
class Task;
class TaskData;

class QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT Solution : public QObject
{
    Q_OBJECT
public:
    Solution();
    Solution(Problem &problem);
    Solution(const Solution &solution);
    ~Solution();

    bool isEmpty() const;
    void clear();
    void operator=(const Solution &solution);

    virtual void solve() = 0;
    virtual double fitness() const;

public:
    Problem &problem;

    QMap<QuandoScheduler::Resource*, QuandoSchedulerScheduler::Resource*> resources;

    QVector<TaskData*> taskSequence;
    QVector<TaskData*> startTasks;
    QVector<TaskData*> endTasks;

Q_SIGNALS:
    void sendMessage(const QString &msg);

protected:
    void addMessage(const QString &msg);
    QuandoSchedulerScheduler::Resource *resource(const QuandoScheduler::Allocation* a);
    double bookResource(TaskData *taskdata, Resource* resource, const QuandoScheduler::Allocation *allocation, int slot);

    TaskData *findTaskData(Task *task) const;

    void copy(const Solution &solution);

protected:
    double m_fitness;

private:
    Problem emptyProblem;
};

} // namespace QuandoScheduler

QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT QDebug operator<<(QDebug dbg, const QuandoSchedulerScheduler::Solution *s);
QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT QDebug operator<<(QDebug dbg, const QuandoSchedulerScheduler::Solution &s);

#endif // QuandoSchedulerScheduler_Solution_h
