/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QuandoSchedulerScheduler_Problem_h
#define QuandoSchedulerScheduler_Problem_h

#include "quandoschedulerschedulerplugin_export.h"

#include <QObject>
#include <QMultiMap>
#include <QDateTime>
#include <QVector>

namespace QuandoScheduler {
    class Resource;
    class Allocation;
    class Task;
}

namespace QuandoSchedulerScheduler {

class Project;
class Task;
class Resource;

class QUANDOSCHEDULERSCHEDULERPLUGIN_EXPORT Problem
{
public:
    explicit Problem();
    ~Problem();

    bool isEmpty() const;
    void clear();

    Task *findTask(QuandoScheduler::Task *qsTask) const;

    QVector<Project*> projects;
    QVector<Task*> startTasks;
    QVector<Task*> endTasks;
    QVector<Task*> tasks;

    QMultiMap<Task*, QMap<QuandoScheduler::Allocation*, int> > allocations; // <Task, QMap<Allocation, resource>>

    QDateTime zeroTime;
    QDateTime maxTime;
    int granularity;
};

} // namespace QuandoScheduler

QDebug operator<<(QDebug dbg, const QuandoSchedulerScheduler::Problem *p);
QDebug operator<<(QDebug dbg, const QuandoSchedulerScheduler::Problem &p);

#endif // QuandoSchedulerScheduler_Problem_h
