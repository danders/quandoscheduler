# TODO: plugin should not be SHARED, but MODULE. Needs to be SHARED because tests link to it -> fix with util lib/objects

include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_SOURCE_DIR}/src/lib
    ${CMAKE_BINARY_DIR}/src/lib
)

set (QuandoScedulerSchedulerPlugin_SRCS
    QuandoSchedulerSchedulerPlugin.cpp
    QuandoSchedulerSchedulerPluginAssignment.cpp
    QuandoSchedulerSchedulerPluginProject.cpp
    QuandoSchedulerSchedulerPluginResource.cpp
    QuandoSchedulerSchedulerPluginTask.cpp
    QuandoSchedulerSchedulerProblem.cpp
    QuandoSchedulerSchedulerSolution.cpp
    QuandoSchedulerSchedulerSolutionQuick.cpp
    QuandoSchedulerSchedulerSolutionPert.cpp
    QuandoSchedulerSchedulerSolutionEvolve.cpp
    debug.cpp
)

add_library(quandoschedulerschedulerplugin SHARED ${QuandoScedulerSchedulerPlugin_SRCS})
generate_export_header(quandoschedulerschedulerplugin)

target_link_libraries(quandoschedulerschedulerplugin
    quandoscheduler
    Qt5::Core
)

get_target_property(QT_PLUGIN_PATH Qt5::Core LOCATION)

install(TARGETS quandoschedulerschedulerplugin DESTINATION lib/plugins/quandoscheduler)

# test
# add_library(quandoschedulerschedulerplugintest SHARED ${QuandoScedulerSchedulerPlugin_SRCS})
# target_link_libraries(quandoschedulerschedulerplugintest
#     quandoscheduler
#     Qt5::Core
# )

add_subdirectory(tests)
