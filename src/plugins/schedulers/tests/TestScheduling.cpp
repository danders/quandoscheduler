/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "TestScheduling.h"

#include <QuandoSchedulerSchedulerPlugin.h>
#include <QuandoSchedulerSchedulerPluginProject.h>
#include <QuandoSchedulerSchedulerPluginTask.h>
#include <QuandoSchedulerSchedulerProblem.h>
#include <QuandoSchedulerSchedulerSolution.h>

#include <QuandoScheduler.h>
#include <QuandoSchedulerMain.h>
#include <QuandoSchedulerProject.h>
#include <QuandoSchedulerTask.h>
#include <QuandoSchedulerResource.h>

#include <QTest>
#include <QVariantMap>
#include <QDateTime>
#include <QStringList>
#include <QDebug>

namespace QuandoSchedulerScheduler {

void TestScheduling::scheduleDuration()
{
    QDateTime targetStart = QDateTime::fromString("2020-11-01T00:00:00", Qt::ISODate);
    QDateTime targetFinish = QDateTime::fromString("2020-11-02T00:00:00", Qt::ISODate);

    QuandoScheduler::Data data;
    data.setGranularity(3600);
    data.setZeroTime(targetStart);
    data.setMaxTime(targetFinish);

    QuandoScheduler::Project *project = data.createProject("P1");
    QVERIFY(project);
    project->setSchedulingRange(targetStart, targetFinish);

    QuandoScheduler::Task *task = data.createTask("T1", "P1");
    QVERIFY(task);
    QCOMPARE(task->project(), project);
    task->setEstimateType(QuandoScheduler::ET_Duration);
    QCOMPARE(task->estimateType(), (int)QuandoScheduler::ET_Duration);
    task->setEstimate(12.0);
    QCOMPARE(task->estimate(), 12.0);


    QuandoSchedulerSchedulerPlugin plugin;
    QVariantMap args;
    args.insert("scheduling", "quick");

    QVERIFY(plugin.initiate(0, data, args));

    plugin.execute();
    qInfo()<<plugin.messages();
    qInfo()<<project<<project->actualStart()<<project->actualFinish();
    qInfo()<<task<<task->actualStart()<<task->actualFinish();

    QCOMPARE(project->actualStart(), targetStart);
    QCOMPARE(project->actualFinish(), targetStart.addSecs(12*data.granularity()));
    QCOMPARE(task->actualStart(), targetStart);
    QCOMPARE(task->actualFinish(), targetStart.addSecs(12*data.granularity()));
}

// void TestScheduling::scheduleDuration()
// {
//     QDateTime targetStart = QDateTime::fromString("2020-11-01T00:00:00", Qt::ISODate);
//     QDateTime targetFinish = QDateTime::fromString("2020-11-02T00:00:00", Qt::ISODate);
//     
//     QuandoScheduler::Data data;
//     data.setGranularity(3600);
//     data.setZeroTime(targetStart);
//     data.setMaxTime(targetFinish);
//     
//     QuandoScheduler::Project *project = data.createProject("P1");
//     QVERIFY(project);
//     project->setSchedulingRange(targetStart, targetFinish);
//     
//     QuandoScheduler::Resource *resource = data.createResource("R1");
//     QVERIFY(resource);
//     resource->setType(QuandoScheduler::RT_Work);
//     resource->setAmount(1.0);
//     resource->addAvailability(QuandoScheduler::AT_Available, targetStart, targetFinish, 1.0);
//     
//     QuandoScheduler::Task *task = data.createTask("T1", "P1");
//     QVERIFY(task);
//     QCOMPARE(task->project(), project);
//     task->setEstimateType(QuandoScheduler::ET_Effort);
//     QCOMPARE(task->estimateType(), (int)QuandoScheduler::ET_Effort);
//     task->setEstimate(12.0);
//     QCOMPARE(task->estimate(), 12.0);
//     
//     QVERIFY(data.createAllocation("P1", "T1", "R1", 1.0));
//     
//     QuandoScheduler::Task *t2 = data.createTask("T2", "P1");
//     QVERIFY(t2);
//     t2->setEstimateType(QuandoScheduler::ET_Duration);
//     QCOMPARE(t2->estimateType(), (int)QuandoScheduler::ET_Duration);
//     t2->setEstimate(2.0);
//     QCOMPARE(t2->estimate(), 2.0);
//     
//     QVERIFY(data.createAllocation("P1", "T2", "R1", 1.0));
//     
//     QVERIFY(data.createDependency("P1", "T1", "T2"));
//     
//     QuandoScheduler::Task *t3 = data.createTask("T3", "P1");
//     QVERIFY(t3);
//     t3->setEstimateType(QuandoScheduler::ET_Length);
//     QCOMPARE(t3->estimateType(), (int)QuandoScheduler::ET_Length);
//     t3->setEstimate(2.0);
//     QCOMPARE(t3->estimate(), 2.0);
//     t3->addAvailability(QuandoScheduler::AT_Available, targetStart.addSecs(16*3600), targetFinish);
//     
//     //     QVERIFY(data.createAllocation("P1", "T3", "R1", 1.0));
//     
//     QVERIFY(data.createDependency("P1", "T2", "T3"));
//     
//     QuandoSchedulerSchedulerPlugin plugin;
//     QVariantMap args;
//     args.insert("scheduling", "quick");
//     
//     QVERIFY(plugin.initiate(0, data, args));
//     
//     plugin.execute();
//     qDebug()<<plugin.messages();
//     qDebug()<<project<<project->actualStart()<<project->actualFinish();
//     qDebug()<<task<<task->actualStart()<<task->actualFinish();
// }

}
QTEST_GUILESS_MAIN(QuandoSchedulerScheduler::TestScheduling)
