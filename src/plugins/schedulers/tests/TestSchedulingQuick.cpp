/* This file is part of the QuandoScheduler project.
   Copyright (C) 2017 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "TestSchedulingQuick.h"

#include <QuandoSchedulerSchedulerPlugin.h>
#include <QuandoSchedulerSchedulerPluginProject.h>
#include <QuandoSchedulerSchedulerPluginTask.h>
#include <QuandoSchedulerSchedulerProblem.h>
#include <QuandoSchedulerSchedulerSolution.h>

#include <QuandoScheduler.h>
#include <QuandoSchedulerData.h>
#include <QuandoSchedulerProject.h>
#include <QuandoSchedulerTask.h>
#include <QuandoSchedulerResource.h>
#include <QuandoSchedulerAvailable.h>
#include <QuandoSchedulerAllocation.h>
#include <QuandoSchedulerAssignment.h>

#include <QTest>
#include <QVariantMap>
#include <QDateTime>
#include <QStringList>
#include <QDebug>

#define HOUR 60*60

using namespace QuandoSchedulerScheduler;

void TestSchedulingQuick::duration()
{
    QDateTime targetStart = QDateTime::fromString("2020-11-01T00:00:00", Qt::ISODate);
    QDateTime targetFinish = QDateTime::fromString("2020-11-02T00:00:00", Qt::ISODate);

    QuandoScheduler::Data data;
    data.setGranularity(3600);
    data.setZeroTime(targetStart);
    data.setMaxTime(targetFinish);

    QuandoScheduler::Project *project = data.createProject("P1");
    QVERIFY(project);
    project->setSchedulingRange(targetStart, targetFinish);

    QuandoScheduler::Task *task = data.createTask("T1", "P1");
    QVERIFY(task);
    QCOMPARE(task->project(), project);
    task->setEstimateType(QuandoScheduler::ET_Duration);
    QCOMPARE(task->estimateType(), (int)QuandoScheduler::ET_Duration);
    task->setEstimate(12.0);
    QCOMPARE(task->estimate(), 12.0);


    QuandoSchedulerSchedulerPlugin plugin;
    QVariantMap args;
    args.insert("scheduling", "quick");

    QVERIFY(plugin.initiate(0, data, args));
    plugin.execute();
    qInfo()<<plugin.messages();
    qInfo()<<project<<project->actualStart()<<project->actualFinish();
    qInfo()<<task<<task->actualStart()<<task->actualFinish();

    QCOMPARE(project->actualStart(), targetStart);
    QCOMPARE(project->actualFinish(), targetStart.addSecs(12*data.granularity()));
    QCOMPARE(task->actualStart(), targetStart);
    QCOMPARE(task->actualFinish(), targetStart.addSecs(12*data.granularity()));

    qInfo()<<"2 tasks:";
    QuandoScheduler::Task *task2 = data.createTask("T2", "P1");
    QVERIFY(task2);
    QCOMPARE(task2->project(), project);
    task2->setEstimateType(QuandoScheduler::ET_Duration);
    QCOMPARE(task2->estimateType(), (int)QuandoScheduler::ET_Duration);
    task2->setEstimate(6.0);
    QCOMPARE(task2->estimate(), 6.0);

    QVERIFY(plugin.initiate(0, data, args));    
    plugin.execute();

    QCOMPARE(project->actualStart(), targetStart);
    QCOMPARE(project->actualFinish(), targetStart.addSecs(12*data.granularity()));
    QCOMPARE(task->actualStart(), targetStart);
    QCOMPARE(task->actualFinish(), targetStart.addSecs(12*data.granularity()));
    QCOMPARE(task2->actualStart(), targetStart);
    QCOMPARE(task2->actualFinish(), targetStart.addSecs(6*data.granularity()));

    qInfo()<<"2 dependent tasks:";
    QVERIFY(data.createDependency("P1", "T1", "T2"));

    QVERIFY(plugin.initiate(0, data, args));    
    plugin.execute();

    QCOMPARE(project->actualStart(), targetStart);
    QCOMPARE(project->actualFinish(), targetStart.addSecs(18*data.granularity()));
    QCOMPARE(task->actualStart(), targetStart);
    QCOMPARE(task->actualFinish(), targetStart.addSecs(12*data.granularity()));
    QCOMPARE(task2->actualStart(), targetStart.addSecs(12*data.granularity()));
    QCOMPARE(task2->actualFinish(), targetStart.addSecs(18*data.granularity()));

    qInfo()<<"With resource allocation:";
    QuandoScheduler::Resource *r1 = data.createResource("R1");
    QVERIFY(r1);
    r1->setType(QuandoScheduler::RT_Work);
    r1->setAmount(1.0);
    r1->addAvailability(QuandoScheduler::AT_Available, targetStart, targetFinish, 1.0);

    QuandoScheduler::Allocation *a1 = data.createAllocation("P1", "T1", "R1", 1.0);
    QVERIFY(a1);

    QVERIFY(plugin.initiate(0, data, args));    
    plugin.execute();

    QCOMPARE(project->actualStart(), targetStart);
    QCOMPARE(project->actualFinish(), targetStart.addSecs(18*data.granularity()));
    QCOMPARE(task->actualStart(), targetStart);
    QCOMPARE(task->actualFinish(), targetStart.addSecs(12*data.granularity()));
    QCOMPARE(task2->actualStart(), targetStart.addSecs(12*data.granularity()));
    QCOMPARE(task2->actualFinish(), targetStart.addSecs(18*data.granularity()));

    QuandoScheduler::Assignment *assignment = data.assignment(task, r1);
    QVERIFY(assignment);
    QVERIFY(assignment->count() == 1);
    QCOMPARE(assignment->from(0), task->actualStart());
    QCOMPARE(assignment->until(0), task->actualFinish());
    QCOMPARE(assignment->amount(0), 1.0);

    QuandoScheduler::Resource *r2 = data.createResource("R2");
    QVERIFY(r2);
    r2->setType(QuandoScheduler::RT_Work);
    r2->setAmount(1.0);
    r2->addAvailability(QuandoScheduler::AT_Available, targetStart, targetFinish, 1.0);
    
    QuandoScheduler::Allocation *a2 = data.createAllocation("P1", "T2", "R2", 1.0);
    QVERIFY(a2);
    
    QVERIFY(plugin.initiate(0, data, args));    
    plugin.execute();

    QCOMPARE(project->actualStart(), targetStart);
    QCOMPARE(project->actualFinish(), targetStart.addSecs(18*data.granularity()));
    QCOMPARE(task->actualStart(), targetStart);
    QCOMPARE(task->actualFinish(), targetStart.addSecs(12*data.granularity()));
    QCOMPARE(task2->actualStart(), targetStart.addSecs(12*data.granularity()));
    QCOMPARE(task2->actualFinish(), targetStart.addSecs(18*data.granularity()));
    
    QuandoScheduler::Assignment *ass1 = data.assignment(task, r1);
    QVERIFY(ass1);
    QVERIFY(ass1->count() == 1);
    QCOMPARE(ass1->from(0), task->actualStart());
    QCOMPARE(ass1->until(0), task->actualFinish());
    QCOMPARE(ass1->amount(0), 1.0);

    QuandoScheduler::Assignment *ass2 = data.assignment(task2, r2);
    QVERIFY(ass2);
    QVERIFY(ass2->count() == 1);
    QCOMPARE(ass2->from(0), task2->actualStart());
    QCOMPARE(ass2->until(0), task2->actualFinish());
    QCOMPARE(ass2->amount(0), 1.0);

}

void TestSchedulingQuick::length()
{
    QDateTime targetStart = QDateTime::fromString("2020-11-01T00:00:00", Qt::ISODate);
    QDateTime targetFinish = QDateTime::fromString("2020-11-02T00:00:00", Qt::ISODate);
    
    QuandoScheduler::Data data;
    data.setGranularity(HOUR);
    data.setZeroTime(targetStart);
    data.setMaxTime(targetFinish);
    
    QuandoScheduler::Project *project = data.createProject("P1");
    QVERIFY(project);
    project->setSchedulingRange(targetStart, targetFinish);
    
//     QuandoScheduler::Resource *resource = data.createResource("R1");
//     QVERIFY(resource);
//     resource->setType(QuandoScheduler::RT_Work);
//     resource->setAmount(1.0);
//     resource->addAvailability(QuandoScheduler::AT_Available, targetStart, targetFinish, 1.0);
    
    QuandoScheduler::Task *task = data.createTask("T1", "P1");
    QVERIFY(task);
    QCOMPARE(task->project(), project);
    task->setEstimateType(QuandoScheduler::ET_Length);
    QCOMPARE(task->estimateType(), (int)QuandoScheduler::ET_Length);
    task->setEstimate(2.0);
    QCOMPARE(task->estimate(), 2.0);

    task->addAvailability(QuandoScheduler::AT_Available, targetStart.addSecs(6*HOUR), targetStart.addSecs(8*HOUR));
    
//     QVERIFY(data.createAllocation("P1", "T1", "R1", 1.0));
//     
//     QuandoScheduler::Task *t2 = data.createTask("T2", "P1");
//     QVERIFY(t2);
//     t2->setEstimateType(QuandoScheduler::ET_Duration);
//     QCOMPARE(t2->estimateType(), (int)QuandoScheduler::ET_Duration);
//     t2->setEstimate(2.0);
//     QCOMPARE(t2->estimate(), 2.0);
//     
//     QVERIFY(data.createAllocation("P1", "T2", "R1", 1.0));
//     
//     QVERIFY(data.createDependency("P1", "T1", "T2"));
//     
//     QuandoScheduler::Task *t3 = data.createTask("T3", "P1");
//     QVERIFY(t3);
//     t3->setEstimateType(QuandoScheduler::ET_Length);
//     QCOMPARE(t3->estimateType(), (int)QuandoScheduler::ET_Length);
//     t3->setEstimate(2.0);
//     QCOMPARE(t3->estimate(), 2.0);
//     t3->addAvailability(QuandoScheduler::AT_Available, targetStart.addSecs(16*HOUR), targetFinish);
    
    //     QVERIFY(data.createAllocation("P1", "T3", "R1", 1.0));
    
//     QVERIFY(data.createDependency("P1", "T2", "T3"));
    
    QuandoSchedulerSchedulerPlugin plugin;
    QVariantMap args;
    args.insert("scheduling", "quick");
    
    QVERIFY(plugin.initiate(0, data, args));
    
    plugin.execute();
    qDebug()<<plugin.messages();
    qDebug()<<project<<project->actualStart()<<project->actualFinish();
    qDebug()<<task<<task->actualStart()<<task->actualFinish();

    QCOMPARE(project->actualStart(), targetStart);
    QCOMPARE(project->actualFinish(), targetStart.addSecs(8*HOUR));
    QCOMPARE(task->actualStart(), targetStart.addSecs(6*HOUR));
    QCOMPARE(task->actualFinish(), targetStart.addSecs(8*HOUR));
}

void TestSchedulingQuick::effort()
{
    QDateTime targetStart = QDateTime::fromString("2020-11-01T00:00:00", Qt::ISODate);
    QDateTime targetFinish = QDateTime::fromString("2020-11-02T00:00:00", Qt::ISODate);
    
    QuandoScheduler::Data data;
    data.setGranularity(HOUR);
    data.setZeroTime(targetStart);
    data.setMaxTime(targetFinish);

    QuandoScheduler::Project *project = data.createProject("P1");
    QVERIFY(project);
    project->setSchedulingRange(targetStart, targetFinish);

    QuandoScheduler::Resource *resource = data.createResource("R1");
    QVERIFY(resource);
    resource->setType(QuandoScheduler::RT_Work);
    resource->setAmount(1.0);
    resource->addAvailability(QuandoScheduler::AT_Available, targetStart, targetFinish, 1.0);
    
    QuandoScheduler::Task *task = data.createTask("T1", "P1");
    QVERIFY(task);
    QCOMPARE(task->project(), project);
    task->setEstimateType(QuandoScheduler::ET_Effort);
    QCOMPARE(task->estimateType(), (int)QuandoScheduler::ET_Effort);
    task->setEstimate(12.0);
    QCOMPARE(task->estimate(), 12.0);

    QuandoScheduler::Allocation *alloc = data.createAllocation("P1", "T1", "R1", 1.0);
    QVERIFY(alloc);
    QCOMPARE(alloc->resource(), resource);
    QCOMPARE(alloc->task(), task);
    QVERIFY(task->allocations().contains(resource));
    QCOMPARE(task->allocations().value(resource), alloc);

    QuandoSchedulerSchedulerPlugin plugin;
    QVariantMap args;
    args.insert("scheduling", "quick");
    
    QVERIFY(plugin.initiate(0, data, args));
    
    plugin.execute();
    qDebug()<<plugin.messages();
    qDebug()<<project<<project->actualStart()<<project->actualFinish();
    qDebug()<<task<<task->actualStart()<<task->actualFinish();
    
    QCOMPARE(project->actualStart(), targetStart);
    QCOMPARE(project->actualFinish(), targetStart.addSecs(12*HOUR));
    QCOMPARE(task->actualStart(), targetStart);
    QCOMPARE(task->actualFinish(), targetStart.addSecs(12*HOUR));
}


QTEST_GUILESS_MAIN(TestSchedulingQuick)
