/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "PspWriterPlugin.h"
#include "debug.h"

#include <QuandoScheduler.h>
#include <QuandoSchedulerData.h>
#include <QuandoSchedulerMain.h>
#include <QuandoSchedulerProject.h>
#include <QuandoSchedulerAllocation.h>
#include <QuandoSchedulerDependency.h>
#include <QuandoSchedulerResource.h>
#include <QuandoSchedulerTask.h>
#include <QuandoSchedulerAssignment.h>

#include <QList>
#include <QTimer>
#include <QThread>
#include <QFileInfo>
#include <QFile>
#include <QTextStream>
#include <QDebug>

using namespace QuandoScheduler;

bool PspWriterPlugin::initiate(Main *controller, Data &data, const QVariantMap &args)
{
    debugPlugin<<args;
    clear();

    m_args = args;
    m_data = &data;

    //TODO check args and data

    if (controller) {
        connect(this, SIGNAL(progress(int)), controller, SLOT(slotJobProgress(int)));
        connect(this, SIGNAL(message(QString)), controller, SLOT(slotJobMessage(QString)));
    }
    m_initiatedOk << !args.value("out").toString().isEmpty();
    if (m_initiatedOk[0]) {
        m_file.setFileName(args.value("out").toString());
    }
    debugPlugin<<m_initiatedOk<<m_file.fileName();
    return m_initiatedOk.value(0);
}

void PspWriterPlugin::execute()
{
    debugPlugin<<Q_FUNC_INFO;
    if (m_initiatedOk.isEmpty()) {
        addMessage("Not initated");
        return;
    }
    if (!m_initiatedOk.at(0)) {
        addMessage("Initiation failed");
        return;
    }
    debugPlugin<<m_args;
    addMessage("Started");
    emit progress(0);
    if (!m_file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        addMessage(QString("Failed to open file: %1").arg(m_file.fileName()));
        return;
    }

    writeProjects();

    m_file.close();

    addMessage("Cleanup");
    addMessage("Finished");
    emit progress(100);
}

int PspWriterPlugin::state() const
{
    debugPlugin;
    return 0;
}

void PspWriterPlugin::addMessage(const QString &msg)
{
//     debugPlugin;
    m_messages << msg;
    emit message(msg);
}

QStringList PspWriterPlugin::messages() const
{
    return m_messages;
}

void PspWriterPlugin::clear()
{
    m_initiatedOk.clear();
    m_messages.clear();
}

void PspWriterPlugin::writeProjects()
{
    debugPlugin;
    QTextStream out(&m_file);
    out << "Number of projects: \t" << m_data->projects().count() << endl;
    for (Project *project : m_data->projects()) {
        out << "\tProject: " << project->objectName() << ": " << project->actualStart().toString(Qt::ISODate) << " - " << project->actualFinish().toString(Qt::ISODate) << endl;
        writeTasks(project, out);
    }
}

void PspWriterPlugin::writeTasks(Project *project, QTextStream &out)
{
    int i = 1;
    for (Task *t : project->tasks()) {
        out << "Task " << t->objectName() << ": " << t->actualStart().toString(Qt::ISODate) << " - " << t->actualFinish().toString(Qt::ISODate) << endl;
        {const QMap<Resource*, Allocation*> a = t->allocations();
        QMap<Resource*, Allocation*>::const_iterator it = a.constBegin();
        if (a.isEmpty()) {
            out << "\t No allocations" << endl;
        } else {
            out << "\t Allocations" << endl;
            for (; it != a.constEnd(); ++it) {
                out << "\t\tResource " << it.key()->objectName() << ": " << it.value()->amount() << " (" << it.key()->amount() << ')' << endl;
            }
        }}
        {const QMultiMap<Resource*, Assignment> a = t->assignments();
        QMultiMap<Resource*, Assignment>::const_iterator it = a.constBegin();
        if (a.isEmpty()) {
            out << "\t No assignments" << endl;
            continue;
        }
        for (; it != a.constEnd(); ++it) {
            out << "\t Assigned:" << endl;
            for (int i = 0; i < it.value().count(); ++i) {
                out << "\t\t" << it.key()->objectName() << ": " << it.value().from(i).toString(Qt::ISODate)<< " - " << it.value().until(i).toString(Qt::ISODate) << it.value().amount(i) << endl;
            }
        }}
    }
}

void PspWriterPlugin::writeResources(QTextStream &out)
{
}

void PspWriterPlugin::writeDependencies(QTextStream &out)
{
    debugPlugin;
}


void PspWriterPlugin::writeAllocations(QTextStream &out)
{
    debugPlugin;
}
