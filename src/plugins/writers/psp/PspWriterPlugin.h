/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PspWriterPlugin_h
#define PspWriterPlugin_h

#include "pspwriterplugin_export.h"


#include <QuandoSchedulerPluginInterface.h>

#include <QObject>
#include <QtPlugin>
#include <QVariantMap>
#include <QFile>

class QTextStream;

namespace QuandoScheduler {
    class Data;
    class Project;
    class Task;
    class Resource;
    class Allocation;
}


class PSPWRITERPLUGIN_EXPORT PspWriterPlugin : public QObject, public QuandoScheduler::PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QuandoScheduler_PluginInterface_iid FILE "PspWriterPlugin.json")
    Q_INTERFACES(QuandoScheduler::PluginInterface)

public:
    bool initiate(QuandoScheduler::Main *controller, QuandoScheduler::Data &data, const QVariantMap &args) override;
    void execute() override;
    int state() const override;

    QStringList messages() const;
    void clear();

Q_SIGNALS:
    void progress(int);
    void message(const QString &msg);

protected:
    bool load();
    void addMessage(const QString &msg);

    void writeProjects();
    void writeTasks(QuandoScheduler::Project *project, QTextStream &out);
    void writeResources(QTextStream &out);
    void writeDependencies(QTextStream &out);
    void writeAllocations(QTextStream &out);

private:
    QVariantMap m_args;
    QuandoScheduler::Data *m_data;
    QList<bool> m_initiatedOk;
    QStringList m_messages;
    QFile m_file;
    QMap<QString, int> m_modes;

};

#endif // PspWriterPlugin_h
