/* This file is part of the QuandoScheduler project.
   Copyright (C) 2016 Dag Andersen <danders@get2net.dk>

   The QuandoScheduler Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either
   version 3.0 of the License, or (at your option) any later version.

   The QuandoScheduler Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; see the file COPYING;
   if not, see <http://www.gnu.org/licenses/>.
*/

#include "PspReaderPlugin.h"
#include "debug.h"

#include <QuandoScheduler.h>
#include <QuandoSchedulerData.h>
#include <QuandoSchedulerMain.h>
#include <QuandoSchedulerProject.h>
#include <QuandoSchedulerAllocation.h>
#include <QuandoSchedulerDependency.h>
#include <QuandoSchedulerResource.h>
#include <QuandoSchedulerTask.h>
#include <QuandoSchedulerAssignment.h>

#include <QList>
#include <QTimer>
#include <QThread>
#include <QFileInfo>
#include <QFile>
#include <QDebug>

using namespace QuandoScheduler;

bool PspReaderPlugin::initiate(Main *controller, Data &data, const QVariantMap &args)
{
    debugPlugin<<args;
    clear();

    m_args = args;
    m_data = &data;

    //TODO check args and data

    if (controller) {
        connect(this, SIGNAL(progress(int)), controller, SLOT(slotJobProgress(int)));
        connect(this, SIGNAL(message(QString)), controller, SLOT(slotJobMessage(QString)));
    }
    m_initiatedOk << false;
    if (!args.value("file").toString().isEmpty()) {
        QFileInfo fi(args.value("file").toString());
        m_initiatedOk[0] = fi.exists();
        if (m_initiatedOk[0]) {
            m_file.setFileName(args.value("file").toString());
        }
    }
    return m_initiatedOk.value(0);
}

bool PspReaderPlugin::load()
{
    bool result = true;
    readProjects();
    readResources();
    readDependencies();
    readAllocations();
    return result;
}


void PspReaderPlugin::execute()
{
    debugPlugin<<Q_FUNC_INFO;
    if (m_initiatedOk.isEmpty()) {
        addMessage("Not initated");
        return;
    }
    if (!m_initiatedOk.at(0)) {
        addMessage("Initiation failed");
        return;
    }
    debugPlugin<<m_args;
    addMessage("Started");
    emit progress(0);

    m_data->setGranularity(3600);
    m_data->setZeroTime(QDateTime::fromString("2019-01-01T00:00:00", Qt::ISODate));
    load();

    addMessage("Cleanup");
    addMessage("Finished");
    emit progress(100);
}

int PspReaderPlugin::state() const
{
    debugPlugin;
    return 0;
}

void PspReaderPlugin::addMessage(const QString &msg)
{
//     debugPlugin;
    m_messages << msg;
    emit message(msg);
}

QStringList PspReaderPlugin::messages() const
{
    return m_messages;
}

void PspReaderPlugin::clear()
{
    m_initiatedOk.clear();
    m_messages.clear();
}

bool PspReaderPlugin::findKeyword(const QString &word)
{
    if (!m_file.isOpen()) {
        if (!m_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            debugPlugin<<"Failed to open file:"<<m_args.value("file").toString();
            return false;
        }
    }
    while (!m_file.atEnd()) {
        QString line = m_file.readLine();
        if (line.startsWith(word)) {
            return true;
        }
    }
    return false;
}

bool PspReaderPlugin::endOfData(const QString &line)
{
    return line.startsWith("**");
}

bool PspReaderPlugin::readProjects()
{
    debugPlugin;
    bool result = false;
    if (findKeyword("PROJECT INFORMATION:")) {
        QString line = m_file.readLine();
        const QStringList headers = line.trimmed().split(' ', QString::SkipEmptyParts);
        debugPlugin<<headers;
        while (!m_file.atEnd()) {
            line = m_file.readLine();
            if (endOfData(line)) {
                break;
            }
            // pronr.  #jobs rel.date duedate tardcost  MPM-Time
            //    1     10      0       17        9       17
            QStringList data = line.trimmed().split(' ', QString::SkipEmptyParts);
            QString pname;
            Project *project = 0;
            if (headers.contains("pronr.")) {
                pname = "P" + data.value(headers.indexOf("pronr."));
                project = m_data->createProject(pname);
                project->setObjectName(pname);
            }
            if (headers.contains("#jobs")) {
                Task *t = m_data->createTask(1, pname);
                t->setObjectName("1");
                int jobs = data.value(headers.indexOf("#jobs")).toInt();
                for (int i = 0; i < jobs; ++i) {
                    t = m_data->createTask(i+2, pname);
                    t->setObjectName(QString::number(i+2));
                }
                t = m_data->createTask(jobs+2, pname);
                t->setObjectName(QString::number(jobs+2));
            }
            int start = data.value(headers.indexOf("rel.date")).toInt();
            QDateTime dt = m_data->zeroTime().addSecs(start*3600);
            project->setTargetStart(dt);
            int hours = data.value(headers.indexOf("duedate")).toInt();
            dt = m_data->zeroTime().addSecs(hours*3600);
            project->setTargetFinish(dt);
            if (dt > m_data->maxTime()) {
                m_data->setMaxTime(dt);
            }
            debugPlugin<<m_data->zeroTime()<<m_data->maxTime()<<":"<<project->targetStart()<<project->targetFinish();
            result = true;
        }
        m_file.close();
    }
    return result;
}

bool PspReaderPlugin::readResources()
{
    debugPlugin;
    bool result = false;
    if (findKeyword("RESOURCEAVAILABILITIES:")) {
        QString line = m_file.readLine();
        const QStringList headers = line.trimmed().split(' ', QString::SkipEmptyParts);
        debugPlugin<<headers;
        while (!m_file.atEnd()) {
            line = m_file.readLine();
            if (endOfData(line)) {
                break;
            }
            QStringList data = line.trimmed().split(' ', QString::SkipEmptyParts);
            // R 1  R 2  N 1  N 2
            // 11    9   42   17
            for (int i = 0; i < data.count(); ++i) {
                QString rname = headers.value(i*2) + headers.value((i*2)+1);
                int amount = data.value(i).toInt();
                Resource *r = m_data->createResource(rname);
                r->setObjectName(rname);
                r->setAmount(amount*100);
                r->addAvailability(AT_Available, r->zeroTime(), r->maxTime(), 1.0);
                if (rname.startsWith('R')) {
                    r->setType(RT_Work);
                } else if (rname.startsWith('N')) {
                    r->setType(RT_Consumable);
                } else if (rname.startsWith('D')) {
                    // TODO
                }
            }
            debugPlugin<<data;
            result = true;
        }
    }
    m_file.close();
    return result;
}

bool PspReaderPlugin::readDependencies()
{
    debugPlugin;
    bool result = false;
    if (findKeyword("PRECEDENCE RELATIONS:")) {
        // jobnr.    #modes  #successors   successors
        //   1        1          3           2   3   4
        QString line = m_file.readLine();
        const QStringList headers = line.trimmed().split(' ', QString::SkipEmptyParts);
        debugPlugin<<headers;
        while (!m_file.atEnd()) {
            line = m_file.readLine();
            if (endOfData(line)) {
                break;
            }
            QStringList data = line.trimmed().split(' ', QString::SkipEmptyParts);
            QString jobnr = data.value(headers.indexOf("jobnr."));
            m_modes.insert(jobnr, data.value(headers.indexOf("#modes")).toInt());
            QString succs = data.value(headers.indexOf("#successors"));
            int pos = headers.indexOf("successors");
            if (succs > 0 && pos > -1) {
                for (int i = 0; i < succs && pos < data.count(); ++i, ++pos) {
                    QString succ = data.value(pos);
                    m_data->createDependency("P1", jobnr, succ);
                    debugPlugin<<jobnr<<"->"<<succ;
                }
                result = true;
            }
        }
    }
    m_file.close();
    return result;
}

bool PspReaderPlugin::readAllocations(const QString &jobnr, QStringList &data, const QStringList &hdrs)
{
    QStringList headers = hdrs;
    int ridx = headers.indexOf("R");
    int nidx = headers.indexOf("N");
    int idx = ridx;
    if (nidx >= 0 && nidx < idx) {
        idx = nidx;
    }
    if (idx < 0) {
        debugPlugin<<"No allocations in data";
        return false;
    }
    for (int i = 0; i < idx; ++i) {
        headers.removeFirst();
        data.removeFirst();
    }
    if (headers.isEmpty() || data.isEmpty()) {
        debugPlugin<<"No allocations in data";
        return false;
    }
    debugPlugin<<"job:"<<jobnr<<headers<<data;
    for (int i = 0; i < data.count(); ++i) {
        int amount = data.value(i).toInt();
        if (amount > 0) {
            QString rname = headers.value(i*2) + headers.value((i*2)+1);
            Allocation *a = m_data->createAllocation("P1", jobnr, rname, amount, true);
            debugPlugin<<a;
        }
    }
}

bool PspReaderPlugin::readModes(const QString &jobnr, QStringList &data, const QStringList &headers)
{
    if (jobnr == 0) {
        return false;
    }
    int modes = m_modes.value(jobnr);
    debugPlugin<<"job:"<<jobnr<<"modes:"<<modes<<"headers:"<<headers<<"data:"<<data;
    int m = 0;
    while (m < modes) {
        debugPlugin<<"mode:"<<m<<data;
        // TODO handle multiple modes
        if (m == 0) {
            Task *t = m_data->findTask(jobnr);
            int dur = data.value(headers.indexOf("duration")).toInt();
            t->setEstimate(dur);
            t->setEstimateType(ET_Duration);
            debugPlugin<<"job:"<<jobnr<<"dur:"<<dur;
            if (dur > 0) {
                readAllocations(jobnr, data, headers);
            }
        }
        ++m;
        if (m < modes) {
            QString line = m_file.readLine();
            if (endOfData(line)) {
                return false;
            }
            data = line.trimmed().split(' ', QString::SkipEmptyParts);
        }
    }
    return true;
}

bool PspReaderPlugin::readAllocations()
{
    debugPlugin;
    bool result = false;
    if (findKeyword("REQUESTS/DURATIONS:")) {
        QString line = m_file.readLine();
        const QStringList headers = line.trimmed().split(' ', QString::SkipEmptyParts);
        debugPlugin<<headers;
        while (!m_file.atEnd()) {
            line = m_file.readLine();
            if (endOfData(line)) {
                break;
            }
            if (line.startsWith("--")) {
                continue;
            }
            // jobnr. mode duration  R 1  R 2  N 1  N 2
            // ------------------------------------------------------------------------
            //   2      1     5       2    0    0    0
            //          2     3       0    3    0    0
            QStringList data = line.trimmed().split(' ', QString::SkipEmptyParts);
            if (!readModes(data.value(headers.indexOf("jobnr.")), data, headers)) {
                // may have hit end of data in some way
                result = false;
                break;
            }
            //debugPlugin<<data;
            result = true;
        }
    }
    m_file.close();
    return result;
}
